document.addEventListener("DOMContentLoaded", function () {
  async function fetchRedditData() {
    const url = 'https://corsproxy.io/?' + encodeURIComponent('https://www.reddit.com/r/kustom/.json');

    try {
      let response = await fetch(url);
      let json = await response.json();

      let children = json.data.children
        .filter(item => item.data.link_flair_text === "Theme" && item.data.thumbnail && item.data.thumbnail !== "self" && item.data.thumbnail !== "default")
        .sort((a, b) => b.data.ups - a.data.ups)
        .slice(0, 20);

      json.data.children = children;

      return json;

    } catch (error) {
      console.error('Error:', error);
      return null;
    }
  }

  fetchRedditData().then(redditData => {
    const sliderContainer = document.querySelector('.owl-carousel');
    document.querySelector('.themes').style.display = 'block';
    for (let child of redditData.data.children) {
      let newCellHTML = `
        <div style="width: 140px">
            <a href="https://reddit.com/${child.data.permalink}" target="_blank">
                <img src="${child.data.thumbnail}" alt="${child.data.title}">
            </a>
            <h5>${child.data.title}</h5>
        </div>
        `;

      sliderContainer.innerHTML += newCellHTML;
    }

    const owl = $(".owl-carousel").owlCarousel({
      margin: 10,
      items: 4,
      loop: true,
      center: false,
      autoWidth: true,
      dots: false,
      lazyLoad: true,
    });
    owl.trigger('next.owl.carousel');
    owl.trigger('next.owl.carousel');
  });
});

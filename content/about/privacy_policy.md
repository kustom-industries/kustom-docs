---
title: Privacy Policy
type: docs
categories:
  - Policies
tags:
  - privacy
  - policy
  - data
  - security
---
# Privacy Policy

Zavasoft S.R.L. built the Kustom apps (KWGT Kustom Widget Maker, KLWP Kustom Wallpaper Maker and KLCK Kustom Lockscreen
Maker) as Freemium apps. This SERVICE is provided by Zavasoft S.R.L. at no cost and is intended for use as is.

This page informs visitors regarding our policies with the collection, use, and disclosure of Personal Information for 
anyone using our Service. By using our Service, you agree to the collection and use of information in accordance with 
this policy. The Personal Information we collect is used for providing and improving the Service. We will not use or 
share your information except as described in this Privacy Policy.

The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at 
Kustom unless otherwise defined in this Privacy Policy.

## Information Collection and Use

For a better experience, while using our Service, we may require you to provide us with certain personally identifiable 
information, including but not limited to location. The information we request will be retained by us and used as 
described in this privacy policy.

The app uses third-party services that may collect information used to identify you.

Link to privacy policy of third-party service providers used by the app:

* [Google Play Services](https://www.google.com/policies/privacy/)
* [AdMob](https://support.google.com/admob/answer/6128543?hl=en)
* [Google Analytics for Firebase](https://firebase.google.com/policies/analytics)
* [Firebase Crashlytics](https://firebase.google.com/support/privacy/)
* [Google API Services (Fitness)](https://developers.google.com/fit/policy)

## Data Deletion Request

If you wish to have your data permanently deleted from our system, we provide an easy and secure way to do so. Please 
visit the [Account Deletion](https://kustom.rocks/account/delete) page. Follow the instructions to submit your data 
deletion request. We prioritize your privacy and ensure the complete removal of your data in compliance with data 
protection laws.

## Health Data Access and Use

Kustom provides a What You See Is What You Get (WYSIWYG) editor, empowering you to create custom visualizations of 
various types of data, including fitness-related metrics. When you utilize the fitness functionalities within our 
editor, or load a preset that leverages these capabilities, our apps access health data solely to enhance and 
personalize your app experience. This includes displaying:

- Active calories burned
- Exercise data
- Distance traveled
- Elevation gained
- Floors climbed
- Heart rate
- Power
- Sleep data
- Speed
- Steps taken
- Total calories burned
- VO2 max

It's important to note that this health data is used only for visualization purposes as per your request within the 
editor and the preset on screen. Kustom does not collect, store, or export your health data. Your data remains under 
your control, and is solely employed to enable you to design and view your own data presentations within the app.

## Our Use of Google API Services

All Kustom apps (KWGT, KLWP, KWCH and KLCK) adhere to the Google API Services User Data Policy, including the Limited 
Use requirements. Our use and transfer of information received from Google APIs to any other app complies with this 
policy. For more details, please visit [Google API Services User Data Policy](https://developers.google.com/terms/api-services-user-data-policy).

## Log Data

We collect data and information (through third-party products) on your phone called Log Data in case of an error in the 
app. This Log Data may include information such as your device's Internet Protocol (“IP”) address, device name, 
operating system version, the configuration of the app when utilizing our Service, the time and date of your use of 
the Service, and other statistics.

## Cookies

Cookies are files with a small amount of data, commonly used as anonymous unique identifiers. These are sent to your 
browser from the websites you visit and stored on your device's internal memory.

Our Service does not explicitly use these “cookies”. However, the app may use third-party code and libraries that use “

## Service Providers

We may employ third-party companies and individuals due to the following reasons:

* To facilitate our Service;
* To provide the Service on our behalf;
* To perform Service-related services; or
* To assist us in analyzing how our Service is used.

We want to inform users of this Service that these third parties have access to your Personal Information. The reason is
to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information
for any other purpose.

## Security

We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means
of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100%
secure and reliable, and we cannot guarantee its absolute security.

## Links to Other Sites

This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site.
Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of
these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of
any third-party sites or services.

## Children’s Privacy

These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable
information from children under 13 years of age. In the case we discover that a child under 13 has provided us with
personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware
that your child has provided us with personal information, please contact us so that we will be able to do necessary
actions.

## Changes to This Privacy Policy

We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any
changes. We will notify you of any changes by posting the new Privacy Policy on this page.

This policy is effective as of 2021-03-08

## Contact Us

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at help@kustom.rocks.


# 隐私政策

Zavasoft S.R.L. 构建了 Kustom 应用程序（KWGT Kustom Widget Maker、KLWP Kustom Wallpaper Maker 和 KLCK Kustom Lockscreen
Maker）作为免费增值应用程序。 本服务由 Zavasoft S.R.L. 提供。 免费且按原样使用。

此页面用于告知访问者有关我们收集、使用和披露个人信息的政策
如果有人决定使用我们的服务的信息。

如果您选择使用我们的服务，则表示您同意收集和使用与本政策相关的信息。
我们收集的个人信息用于提供和改进服务。 我们不会使用或分享您的
与任何人的信息，但本隐私政策中所述的除外。

本隐私政策中使用的术语与我们的条款和条件具有相同的含义，可访问
Kustom 除非本隐私政策另有规定。

## 我们对Google API服务的使用
我们遵守Google API服务用户数据政策，包括有限使用要求。 我们将从Google API收到的信息的使用和转移符合此政策。 有关详细信息，请访问 
[Google API服务用户数据政策](https://developers.google.com/terms/api-services-user-data-policy)。

### 数据删除请求

如果您希望从我们的系统中永久删除您的数据，我们提供了一种简单且安全的方法。请访问[账户删除](https://kustom.rocks/account/delete)页面。
按照说明提交您的数据删除请求。我们重视您的隐私，并确保根据数据保护法规彻底删除您的数据。

## 信息收集和使用

为了获得更好的体验，在使用我们的服务时，我们可能会要求您向我们提供某些个人身份信息
信息，包括但不限于位置。 我们要求的信息将由我们保留并用作
在本隐私政策中描述。

该应用程序确实使用第三方服务，这些服务可能会收集用于识别您身份的信息。

## 日志数据

我们想通知您，无论何时您使用我们的服务，如果应用程序出现错误，我们都会收集数据并
您手机上的信息（通过第三方产品）称为日志数据。 此日志数据可能包括以下信息
您的设备互联网协议（“IP”）地址、设备名称、操作系统版本、应用程序配置
使用我们的服务、您使用服务的时间和日期以及其他统计数据。

### 健康数据的访问与使用

当您在我们的编辑器中使用健身功能或加载使用这些功能的预设时，我们的应用会访问和使用健康数据，以增强您的应用体验。这些数据包括：

- 活跃卡路里消耗
- 锻炼数据
- 行走距离
- 爬升高度
- 爬楼层数
- 心率
- 功率
- 睡眠数据
- 速度
- 走过的步数
- 总卡路里消耗
- VO2最大值

这些健康数据用于提供个性化的健身和健康见解，并改善整体应用功能。我们致力于确保这些敏感数据的保密性和安全性。数据不会被收集，集成仅用于在请求时显示数据。

## 饼干

Cookie 是包含少量数据的文件，通常用作匿名唯一标识符。 这些被发送到
来自您访问的网站的浏览器，这些信息存储在您设备的内存中。

本服务不明确使用这些“cookies”。 但是，该应用程序可能会使用第三方代码和使用的库
“cookies”来收集信息和改进他们的服务。 您可以选择接受或拒绝这些
cookie 并知道何时将 cookie 发送到您的设备。 如果您选择拒绝我们的 cookie，您可能无法
使用本服务的某些部分。

## 服务供应商

由于以下原因，我们可能会雇用第三方公司和个人：

* 方便我们的服务；
* 代表我们提供服务；
* 执行与服务相关的服务； 或者
* 协助我们分析我们的服务是如何被使用的。

我们想通知此服务的用户，这些第三方可以访问您的个人信息。 原因是
代表我们执行分配给他们的任务。 但是，他们有义务不披露或使用这些信息
用于任何其他目的。

## 安全

我们重视您向我们提供您的个人信息的信任，因此我们正在努力使用商业上可接受的方式
保护它。 但请记住，没有任何一种网络传输方法或电子存储方法是 100% 的
安全可靠，我们不能保证其绝对安全。

## 链接到其他网站

本服务可能包含其他网站的链接。 如果您单击第三方链接，您将被定向到该站点。
请注意，这些外部网站并非由我们运营。 因此，我们强烈建议您查看隐私政策
这些网站。 我们无法控制内容、隐私政策或做法，也不承担任何责任
任何第三方网站或服务。

## 儿童隐私

这些服务不面向 13 岁以下的任何人。我们不会故意收集个人身份信息
来自 13 岁以下儿童的信息。 如果我们发现 13 岁以下的儿童向我们提供了
个人信息，我们会立即将其从我们的服务器中删除。 如果您是父母或监护人并且您知道
如果您的孩子向我们提供了个人信息，请与我们联系，以便我们能够做必要的
动作。

## 本隐私政策的变更

我们可能会不时更新我们的隐私政策。 因此，建议您定期查看此页面以了解任何
变化。 我们将通过在此页面上发布新的隐私政策来通知您任何更改。

本政策自2021-03-08起生效

## 联系我们

如果您对我们的隐私政策有任何疑问或建议，请随时通过 help@kustom.rocks 与我们联系。

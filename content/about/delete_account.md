---
title: Account Removal
type: docs
categories:
  - Account
tags:
  - account
  - gallery
  - konsole
---
# How to Request Removal of Your Account Data

We take our users' privacy seriously at Kustom Industries. If you would like to have your account data removed from our apps, please follow the steps below:

1. Send an email to our support team at help@kustom.rocks requesting the removal of your account data. 
2. In your email, please include the following information:
   - Your full name 
   - The email address associated with your account
   - A brief description of why you are requesting the removal of your account data
3. Once we receive your email, we will confirm your identity and proceed with removing your account data along with any associated content. This means all data **across all of our apps** 

Please note that after your account data is removed, you will no longer be able to access your Kustom account or any associated content.

If you have any questions or concerns regarding the removal of your account data, please do not hesitate to reach out to our support team at help@kustom.rocks.

Thank you for choosing us.
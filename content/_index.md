---
title: Kustom HQ
weight: 1
---

# Kustom HQ

Welcome to Kustom Industries HQ! Yes Kustom can!

## What do you do?

At Kustom industries we believe that homescreens will need to look awesome when Aliens will come, this is why we do 
<a href="#get-the-apps">top notch tools</a> for that. But a picture is worth a thousand words right? So check out last 
month greatest creations from our awesome and always growing <a
href="https://www.reddit.com/r/kustom/search/?q=THEME&restrict_sr=1&sr_nsfw=&include_over_18=1&t=month&sort=top"
target="_blank">Reddit Community</a>, join the revolution!

{{< topthemes >}}

## Rock with Kustom

{{< col >}}

### Get the apps

Go get Kustom now, best thing to do is to start playing with it. You can download our apps from:

-   <a href="https://kustom.rocks/store" target="_blank">Google Play Store</a>: all apps, recommended channel
-   Huawei App Gallery: 
    <a href="https://appgallery.huawei.com/app/C102575177" target="_blank">KWGT</a>,
    <a href="https://appgallery.huawei.com/app/C102587157" target="_blank">KLWP</a> (stable only)
-   Manually from our [downloads page]({{< ref "/docs/downloads" >}})

<--->

### Be involved

Need help? Wanna discuss new things? Kustom is 10% software 90% our great community!

-   <a href="https://reddit.com/r/Kustom" target="_blank">Official Sub Reddit</a>
-   <a href="https://kustom.rocks/discord" target="_blank">Discord</a>
-   <a href="https://kustom.rocks/ideas" target="_blank">Feature Requests</a>
-   <a href="https://kustom.rocks/probles" target="_blank">Issues Forums</a>

{{< /col >}}

## Spotlight

{{< col >}}

### KWGT

{{< img src="/images/ic_kwgt.png" alt="KLWP" size="96x96" float="left" >}} Create Android widgets your way, not only
clocks but calendars, music widgets, home automation dashboards, system information screens, basically anything, just
have a look at our functions and see what you can do

<--->

### KLWP

{{< img src="/images/ic_klwp.png" alt="KLWP" size="96x96" float="left" >}} Push your customization to the limit with 
the most advanced live wallpaper creator in da house! Add animations to your data in the most creatives way, make your
home screen interactive exactly as you imagined it

{{< /col >}}

## Examples

As you know our motto is "Kustom Can", in our recipes section on the left menu here you can find a set of example and
common use cases. Do you want us to make a tutorial for you? Just let us know [via email](mailto:help@kustom.rocks). In
the meanwhile check these out:

{{< series recipes >}}

## Localization

Kustom is translated by our awesome community with the help of [Localazy](https://localazy.com/p/Kustom?ref=a9KbDRriyKnY-etX) platform, check it out now and help us reaching more languages!

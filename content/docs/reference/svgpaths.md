---
title: "SVG Paths in Shapes"
type: docs
categories:
  - Reference
tags:
  - svg
  - advanced
  - path
---
# Shapes path

From version 3.73 onwards users can use custom SVG paths in Shapes, just **add a Shape module and select "Path" as type**. This powerful feature allows creating your own shapes while maintaining all the great functionalities of Shapes, such as gradients, masks, and bitmaps. Furthermore, formulas can be used inside the path definition, offering even more customization options! You could use this to generate a graph for example.

## SVG Path Basics

SVG (Scalable Vector Graphics) is an XML-based vector image format for two-dimensional graphics. In the context of Kustom apps, SVG paths are a way of defining custom shapes.

Here's a simple example of an SVG path that draws a line from the upper left corner (0,0) to the lower right one (100,100):

```svg
L 100 100
```

When using SVG paths in Kustom apps, the path is moved to (0,0) and always uses a 100x100 viewport. This means that the path is scaled to fit the shape size. Path is not closed by default, you need to add a "Z" at the end if you want to.

## Testing Your SVG Path

Before using your SVG path in Kustom, you might want to visualize it first to make sure it looks as expected. You can do this using the [SVG Path Visualizer](https://svg-path-visualizer.netlify.app).

Simply paste your SVG path into the text box and the tool will draw it for you. This is a great way to test and fine-tune your paths before using them in Kustom.
---
title: Global Variables
type: docs
categories:
 - Reference
tags:
 - functions
 - formula
 - globals
 - gv
---
# What are Global Variables?
Global Variables (GV) in Kustom are user-defined settings that can be applied across multiple modules or components simultaneously. They allow you to control various aspects of your design from a central location, making it easier to maintain consistency and implement theme-wide changes quickly.

# How to use Globals
If you open the Advanced Editor, you'll notice a tab called "globals" on the first "root" container. This section allows you to add, remove, and modify Global variables.

## Basic Usage Example
Let's say you have a preset with multiple text items and want to change the font of all these items from a single control point:

1. Create a Global of type "Font" in the Globals section
2. Go to a TextModule, select the "Font" preference
3. Click on the "Globe" icon in the action bar at the top
4. Select the Global you just created from the list
5. Repeat for all Text modules you want to control with that global

Now you can change the font of all connected modules by simply updating the Global preference in the main container.

# Global Variable Types
Kustom supports various types of Global Variables including:
- Text
- Number
- Color
- Toggle
- List
- Image
- Font

# Global persistency
Starting with version 3.78 global Variables now have enhanced persistence capabilities:

- Globals persist across device reboots
- Globals persist across preset exports and imports (if you set a global in preset A, export it, load preset B, then load preset A again, your globals will be restored)
- By default, when you change a global in the editor, the global on the preset will also be changed
- This behavior can be modified in the app's general settings if you prefer globals not to be automatically updated, in the settings you can decide weather the editor always overwrites the preset or never does

# Using Globals in Formulas
You can reference Global Variables in formulas using the `$gv(name)$` syntax, where "name" is the name of your global, you can optionally provide a default as a second parameter that will be used when no value is set so  `$gv(name, default)$`

# Best Practices
- Use descriptive names for your globals to easily identify their purpose
- Create globals for elements that might change across themes (colors, fonts, sizes)
- Consider using globals for information that needs to be updated frequently
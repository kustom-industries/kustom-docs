---
title: WF - weather forecast
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - wf
  - weather forecast
---
<h1>WF: weather forecast (max/min temperature, chance of rain, conditions…)</h1>

<h3>Syntax</h3>
wf(type, day, [hours])
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
  <li><b>day</b>: Forecast day index (0 is today)</li>
  <li><b>hours</b>: Forecast hours offset (for hourly weather, 3 means 3 hours from now plus days * 24)</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$wf(min, 0)$&#176;$wi(tempu)$</b></td>
    <td>Today's min temperature in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(max, 0)$&#176;$wi(tempu)$</b></td>
    <td>Today's max temperature in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(cond, 0)$</b></td>
    <td>Today's forecast condition</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(icon, 0)$</b></td>
    <td>Today's forecast icon, one of: &#10;UNKNOWN, &#10;TORNADO, &#10;TSTORM, &#10;TSHOWER, &#10;SHOWER, &#10;RAIN, &#10;SLEET, &#10;LSNOW, &#10;SNOW, &#10;HAIL, &#10;FOG, &#10;WINDY, &#10;PCLOUDY, &#10;MCLOUDY, &#10;CLEAR</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(code, 0)$</b></td>
    <td>Today's forecast code, one of: &#10;TORNADO, &#10;TROPICAL_STORM, &#10;HURRICANE, &#10;SEVERE_THUNDERSTORMS, &#10;THUNDERSTORMS, &#10;MIXED_RAIN_SNOW, &#10;MIXED_RAIN_SLEET, &#10;MIXED_SNOW_SLEET, &#10;FREEZING_DRIZZLE, &#10;DRIZZLE, &#10;FREEZING_RAIN, &#10;SHOWERS, &#10;HEAVY_SHOWERS, &#10;SNOW_FLURRIES, &#10;LIGHT_SNOW_SHOWERS, &#10;BLOWING_SNOW, &#10;SNOW, &#10;HAIL, &#10;SLEET, &#10;DUST, &#10;FOGGY, &#10;HAZE, &#10;SMOKY, &#10;BLUSTERY, &#10;WINDY, &#10;CLOUDY, &#10;MOSTLY_CLOUDY, &#10;PARTLY_CLOUDY, &#10;CLEAR, &#10;FAIR, &#10;MIXED_RAIN_AND_HAIL, &#10;ISOLATED_THUNDERSTORMS, &#10;SCATTERED_SHOWERS, &#10;HEAVY_SNOW, &#10;SCATTERED_SNOW_SHOWERS, &#10;THUNDERSHOWERS, &#10;SNOW_SHOWERS, &#10;ISOLATED_THUNDERSHOWERS, &#10;NOT_AVAILABLE</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(rainc, 0)$%</b></td>
    <td>Today's chance of rain in percentage (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(rain, 0)$mm</b></td>
    <td>Today's precipitations in mm (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(temp, 0, 0)$&#176;$wi(tempu)$</b></td>
    <td>Next hour temperature in local unit (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(cond, 0, 3)$</b></td>
    <td>Weather condition 3 hours from now (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", wf(start, 0, 4))$ - $df("hh:mma", wf(end, 0, 4))$</b></td>
    <td>Start - End of validity of hourly forecast 4 hours from now (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(rainc, 0, 12)$%</b></td>
    <td>Chance 12 hours from now in percentage (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(wchill, 0)$$tc(utf, b0)$$wi(tempu)$</b></td>
    <td>Wind Chill in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(wspeed, 0)$$li(spdu)$</b></td>
    <td>Wind Speed in local unit (kmh/mph)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(wspeedm, 0)$mps</b></td>
    <td>Wind Speed in meters per second</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(wdir, 0)$</b></td>
    <td>Wind Direction in degrees</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(hum, 0)$%</b></td>
    <td>Current humidity in percent</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(clouds, 0)$%</b></td>
    <td>Current cloud coverage in percent (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(press, 0)$mbar</b></td>
    <td>Current pressure in Millibars</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(minc, 0)$&#176;C</b></td>
    <td>Today's min temperature in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$wf(maxc, 0)$&#176;C</b></td>
    <td>Today's max temperature in local unit</td>
  </tr>
</table>

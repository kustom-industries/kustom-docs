---
title: MQ - music queue
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - mq
  - music queue
---
<h1>MQ: music queue (get current playlist or next track name, duration…)</h1>

<h3>Syntax</h3>
mq(type, [index])
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Track Index (len for playlist length)</li>
  <li><b>index</b>: Info type, see examples</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$mq(title, mi(track) + 1)$</b></td>
    <td>Next track title (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mq(sub, mi(track) - 1)$</b></td>
    <td>Prev track sub title (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mq(len)$</b></td>
    <td>Playlist length</td>
  </tr>
</table>

---
title: BP - bitmap palette
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - bp
  - bitmap palette
---
<h1>BP: bitmap palette (extracts colors from images like cover art or local pictures)</h1>

<h3>Syntax</h3>
bp(mode, bitmap, [default])
<h3>Arguments</h3>
<ul>
  <li><b>mode</b>: Color to extract (see examples)</li>
  <li><b>bitmap</b>: Cover art, bitmap global, http resource or full path to a file</li>
  <li><b>default</b>: Default color to use when nothing is found</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$bp(muted, mi(cover))$</b></td>
    <td>Extract muted color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(vibrant, mi(cover))$</b></td>
    <td>Extract vibrant color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(dominant, mi(cover))$</b></td>
    <td>Extract dominant color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(mutedbc, mi(cover))$</b></td>
    <td>Extract muted body text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(vibrantbc, mi(cover))$</b></td>
    <td>Extract vibrant body text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(mutedtc, mi(cover))$</b></td>
    <td>Extract muted title text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(vibranttc, mi(cover))$</b></td>
    <td>Extract vibrant title text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(dmuted, mi(cover))$</b></td>
    <td>Extract muted dark color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(dvibrant, mi(cover))$</b></td>
    <td>Extract vibrant dark color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(dmutedbc, mi(cover))$</b></td>
    <td>Extract muted dark body text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(dvibrantbc, mi(cover))$</b></td>
    <td>Extract vibrant dark body text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(dmutedtc, mi(cover))$</b></td>
    <td>Extract muted dark title text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(dvibranttc, mi(cover))$</b></td>
    <td>Extract vibrant dark title text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(lmuted, mi(cover))$</b></td>
    <td>Extract muted light color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(lvibrant, mi(cover))$</b></td>
    <td>Extract vibrant light color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(lmutedbc, mi(cover))$</b></td>
    <td>Extract muted light body text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(lvibrantbc, mi(cover))$</b></td>
    <td>Extract vibrant light body text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(lmutedtc, mi(cover))$</b></td>
    <td>Extract muted light title text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(lvibranttc, mi(cover))$</b></td>
    <td>Extract vibrant light title text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(dominanttc, mi(cover))$</b></td>
    <td>Extract dominant title text color from cover art</td>
  </tr>
  <tr>
    <td width="250px"><b>$bp(dominantbc, mi(cover))$</b></td>
    <td>Extract dominant body text color from cover art</td>
  </tr>
</table>

---
title: LV - local variables
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - lv
  - local variables
---
<h1>LV: local variables (get and set local variable values)</h1>

<h3>Syntax</h3>
lv()
<h3>Arguments</h3>
<ul>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$lv("foo", "Some Value")$Value is: $lv(foo)$</b></td>
    <td>Sets a simple variable and shows value using lv()</td>
  </tr>
  <tr>
    <td width="250px"><b>$lv("foo", df(m))$$"Minutes are: "+#foo$</b></td>
    <td>Sets a simple variable and shows value using #</td>
  </tr>
</table>

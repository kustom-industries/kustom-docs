---
title: WI - current weather
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - wi
  - current weather
---
<h1>WI: current weather (temperature, rain, conditions…)</h1>

<h3>Syntax</h3>
wi(type)
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$wi(temp)$&#176;$wi(tempu)$</b></td>
    <td>Temperature in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(flik)$&#176;$wi(tempu)$</b></td>
    <td>Feels Like temperature (Heat Index) in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(dpoint)$&#176;$wi(tempu)$</b></td>
    <td>Dew Point in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(fpoint)$&#176;$wi(tempu)$</b></td>
    <td>Frost Point in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(cond)$</b></td>
    <td>Current weather condition</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(icon)$</b></td>
    <td>Current weather icon, one of: &#10;UNKNOWN, &#10;TORNADO, &#10;TSTORM, &#10;TSHOWER, &#10;SHOWER, &#10;RAIN, &#10;SLEET, &#10;LSNOW, &#10;SNOW, &#10;HAIL, &#10;FOG, &#10;WINDY, &#10;PCLOUDY, &#10;MCLOUDY, &#10;CLEAR</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(code)$</b></td>
    <td>Current weather code, one of: &#10;TORNADO, &#10;TROPICAL_STORM, &#10;HURRICANE, &#10;SEVERE_THUNDERSTORMS, &#10;THUNDERSTORMS, &#10;MIXED_RAIN_SNOW, &#10;MIXED_RAIN_SLEET, &#10;MIXED_SNOW_SLEET, &#10;FREEZING_DRIZZLE, &#10;DRIZZLE, &#10;FREEZING_RAIN, &#10;SHOWERS, &#10;HEAVY_SHOWERS, &#10;SNOW_FLURRIES, &#10;LIGHT_SNOW_SHOWERS, &#10;BLOWING_SNOW, &#10;SNOW, &#10;HAIL, &#10;SLEET, &#10;DUST, &#10;FOGGY, &#10;HAZE, &#10;SMOKY, &#10;BLUSTERY, &#10;WINDY, &#10;CLOUDY, &#10;MOSTLY_CLOUDY, &#10;PARTLY_CLOUDY, &#10;CLEAR, &#10;FAIR, &#10;MIXED_RAIN_AND_HAIL, &#10;ISOLATED_THUNDERSTORMS, &#10;SCATTERED_SHOWERS, &#10;HEAVY_SNOW, &#10;SCATTERED_SNOW_SHOWERS, &#10;THUNDERSHOWERS, &#10;SNOW_SHOWERS, &#10;ISOLATED_THUNDERSHOWERS, &#10;NOT_AVAILABLE</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(wspeed)$$li(spdu)$</b></td>
    <td>Wind Speed in local unit (kmh/mph)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(wspeedm)$mps</b></td>
    <td>Wind Speed in meters per second</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(wchill)$$tc(utf, b0)$$wi(tempu)$</b></td>
    <td>Wind Chill in local unit</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(wdir)$</b></td>
    <td>Wind Direction in degrees</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(press)$mbar</b></td>
    <td>Current pressure in Millibars</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(hum)$%</b></td>
    <td>Current humidity in percent</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(clouds)$%</b></td>
    <td>Current cloud coverage in percent (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(uvindex)$</b></td>
    <td>Current UV index (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(tempc)$&#176;C</b></td>
    <td>Temperature in centigrades</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(provider)$</b></td>
    <td>Weather provider used to fetch data</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", wi(updated))$</b></td>
    <td>Time of last weather update in hh:mm format</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(lid)$</b></td>
    <td>Weather station ID (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(pdays)$</b></td>
    <td>Number of days of forecast</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(prain)$</b></td>
    <td>Provider precipitation support, 1 if available, 0 if not</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(prainc)$</b></td>
    <td>Provider rain chance support, 1 if available, 0 if not</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(phours)$</b></td>
    <td>Number of hours in the hourly forecast if available</td>
  </tr>
  <tr>
    <td width="250px"><b>$wi(phstep)$</b></td>
    <td>Provider length of hourly forecast entries, in hours, usually 1</td>
  </tr>
</table>

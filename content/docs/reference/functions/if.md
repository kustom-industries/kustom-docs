---
title: IF - if conditions
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - if
  - if conditions
---
<h1>IF: if conditions (if/then/else support with multiple boolean operators)</h1>

<h3>Syntax</h3>
if(condition, then, [else])
<h3>Arguments</h3>
<ul>
  <li><b>condition</b>: A condition can use any comparison like = (equals), > (greater), >= (greater or equal), < (less), <= (less or equal) combined with boolean operators & (AND) or | (OR) and parenthesis</li>
  <li><b>then</b>: Text or function to use if condition is true (so if return value is not empty and not 0)</li>
  <li><b>else</b>: Optional text or function to be called if condition is false (so either empty or 0)</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$if(df(f)&gt;5, "Week End!", "Workday :(")$</b></td>
    <td>Will show Week End! during week ends or Workday :( during workdays</td>
  </tr>
  <tr>
    <td width="250px"><b>Battery $if(bi(level) = 100, "is fully charged", bi(level) &lt;= 15, "is critically low", "is at " + bi(level) + "%" )$</b></td>
    <td>Shows status of battery writing fully charged when full, critical if below 15 or the normal level otherwise</td>
  </tr>
  <tr>
    <td width="250px"><b>$if(df(MMMM) ~= "a", "Has an A", NO)$</b></td>
    <td>Will show Has an A if this month name contains an a (regexp allowed), NO otherwise</td>
  </tr>
  <tr>
    <td width="250px"><b>$if(wi(code) != CLEAR, "Not Sunny")$</b></td>
    <td>Will show not sunny if sky is not clear, nothing otherwise</td>
  </tr>
</table>

---
title: NI - system notification
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - ni
  - system notification
---
<h1>NI: system notification (status bar notification data like title, icon, line count…)</h1>

<h3>Syntax</h3>
ni(type)
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$ni(count)$</b></td>
    <td>Cancellable notifications count</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(scount)$</b></td>
    <td>Persistent notifications count</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(pcount, com.facebook.orca)$</b></td>
    <td>Facebook messenger notifications count</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(pcount, com.whatsapp)$</b></td>
    <td>Whatsapp notifications count</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(pcount, com.google.android.gm)$</b></td>
    <td>Gmail notifications count</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(0, title)$</b></td>
    <td>Title of first cancellable notification</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(0, text)$</b></td>
    <td>Short text of first cancellable notification</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(0, desc)$</b></td>
    <td>Long text of first cancellable notification</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(0, icon)$</b></td>
    <td>Icon of first cancellable notification (to be used as Bitmap formula)</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(0, bicon)$</b></td>
    <td>Large icon of first cancellable notification (to be used as Bitmap formula)</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(0, count)$</b></td>
    <td>Lines count of first cancellable notification</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(0, pkg)$</b></td>
    <td>Pkg of first cancellable notification</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(0, app)$</b></td>
    <td>App name of first cancellable notification</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(ni(0, time))$</b></td>
    <td>Time since first cancellable notification</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(s0, text)$</b></td>
    <td>Short text of first persistent notification</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(com.google.android.gm, text)$</b></td>
    <td>Text of first Gmail notification (if there)</td>
  </tr>
</table>

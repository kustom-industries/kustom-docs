---
title: MI - music info
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - mi
  - music info
---
<h1>MI: music info (playing track, artist, album cover…)</h1>

<h3>Syntax</h3>
mi(type)
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$mi(album)$</b></td>
    <td>Current Album (if set)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mi(artist)$</b></td>
    <td>Current Artist (if set)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mi(title)$</b></td>
    <td>Current Track Title (if set)</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(mi(len), mm:ss)$</b></td>
    <td>Current Track Duration (in mm:ss format)</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(mi(pos), mm:ss)$</b></td>
    <td>Current Track Position (in mm:ss format)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mi(len)$</b></td>
    <td>Current Track Duration (in seconds)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mi(pos)$</b></td>
    <td>Current Track Position (in seconds)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mi(vol)$</b></td>
    <td>Current Music Volume (0 to 100)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mi(percent)$</b></td>
    <td>Current Track Position (in percentage)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mi(cover)$</b></td>
    <td>Current Cover Image (to be used in Image module or Background as formula)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mi(package)$</b></td>
    <td>Current Player Package Name</td>
  </tr>
  <tr>
    <td width="250px"><b>$mi(track)$</b></td>
    <td>Current track in playlist (if available, to be used with mq)</td>
  </tr>
  <tr>
    <td width="250px"><b>$ni(mi(package), bicon)$</b></td>
    <td>Current Player App Icon (to be used in Image module or Background as formula)</td>
  </tr>
  <tr>
    <td width="250px"><b>$mi(state)$</b></td>
    <td>Current Player State, one of:: &#10;STOPPED, &#10;PAUSED, &#10;PLAYING, &#10;FORWARDING, &#10;REWINDING, &#10;SKIPPING_FORWARDS, &#10;SKIPPING_BACKWARDS, &#10;BUFFERING, &#10;ERROR, &#10;NONE</td>
  </tr>
</table>

---
title: AI - astronomical info
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - ai
  - astronomical info
---
<h1>AI: astronomical info (calculate sunrise, sunset, zodiac and moon data for a location)</h1>

<h3>Syntax</h3>
ai(type)
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ai(sunrise))$</b></td>
    <td>Today's sunrise in hh:mm format</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ai(sunset))$</b></td>
    <td>Today's sunset in hh:mm format</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(isday)$</b></td>
    <td>Will return 1 during daylight or 0 if night</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(ai(nsunrise))$</b></td>
    <td>Time to next sunrise</td>
  </tr>
  <tr>
    <td width="250px"><b>$tf(ai(nsunset))$</b></td>
    <td>Time to next sunset</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(mphase)$</b></td>
    <td>Current Moon phase name</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(zodiac)$</b></td>
    <td>Current Zodiac sign name</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(season)$</b></td>
    <td>Current Season name</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(mage)$</b></td>
    <td>Current Moon age</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(mill)$%</b></td>
    <td>Current Moon illumination (in percentage)</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ai(moonrise))$</b></td>
    <td>Moon rise</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(moonrise, a1d)$</b></td>
    <td>Moon rise for tomorrow</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ai(moonset))$</b></td>
    <td>Moon set</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(moonrise, r2d)$</b></td>
    <td>Moon set 2 days ago</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ai(csunrise))$</b></td>
    <td>Today's civil sunrise in hh:mm format</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ai(csunset))$</b></td>
    <td>Today's civil sunset in hh:mm format</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ai(usunrise))$</b></td>
    <td>Today's nautical sunrise in hh:mm format</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ai(usunset))$</b></td>
    <td>Today's nautical sunset in hh:mm format</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ai(asunrise))$</b></td>
    <td>Today's astronomical sunrise in hh:mm format</td>
  </tr>
  <tr>
    <td width="250px"><b>$df("hh:mma", ai(asunset))$</b></td>
    <td>Today's astronomical sunset in hh:mm format</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(mphasec)$</b></td>
    <td>Current Moon phase code, one of: &#10;NEW, &#10;WAXING_CRESCENT, &#10;FIRST_QUARTER, &#10;WAXING_GIBBOUS, &#10;FULL, &#10;WANING_GIBBOUS, &#10;THIRD_QUARTER, &#10;WANING_CRESCENT</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(zodiacc)$</b></td>
    <td>Current Zodiac sign code, one of: &#10;ARIES, &#10;TAURUS, &#10;GEMINI, &#10;CANCER, &#10;LEO, &#10;VIRGO, &#10;LIBRA, &#10;SCORPIO, &#10;SAGITTARIUS, &#10;CAPRICORN, &#10;AQUARIUS, &#10;PISCES</td>
  </tr>
  <tr>
    <td width="250px"><b>$ai(seasonc)$</b></td>
    <td>Current Season code, one of: &#10;SPRING, &#10;SUMMER, &#10;AUTUMN, &#10;WINTER</td>
  </tr>
</table>

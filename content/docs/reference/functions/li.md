---
title: LI - location info
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - li
  - location info
---
<h1>LI: location info (latitude, longitude, address…)</h1>

<h3>Syntax</h3>
li(type)
<h3>Arguments</h3>
<ul>
  <li><b>type</b>: Info type, see examples</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$li(loc)$</b></td>
    <td>Current Locality (ex Hill Valley, if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(country)$</b></td>
    <td>Current Country Name (ex Iceland, if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(ccode)$</b></td>
    <td>Current Country Code (ex US, if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(addr)$</b></td>
    <td>Current Address (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(admin)$</b></td>
    <td>Current Admin Area (ex CA, if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(neigh)$</b></td>
    <td>Current Neighborhood (if available, locality otherwise)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(postal)$</b></td>
    <td>Current Postal Code (if available)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(spd)$</b></td>
    <td>Current Speed in local unit (kmh/mph if available, 0 otherwise)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(spdm)$</b></td>
    <td>Current Speed in meters per second (if available, 0 otherwise)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(spdu)$</b></td>
    <td>Current Speed unit (kmh/mph)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(alt)$</b></td>
    <td>Altitude in local unit (meters/feet with GPS lock only, 0 otherwise)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(altm)$</b></td>
    <td>Altitude in meters (with GPS lock only, o otherwise)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(lat)$</b></td>
    <td>Latitude</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(lon)$</b></td>
    <td>Longitude</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(lplat)$</b></td>
    <td>Latitude (low precision ~50m)</td>
  </tr>
  <tr>
    <td width="250px"><b>$li(lplon)$</b></td>
    <td>Longitude (low precision ~50m)</td>
  </tr>
</table>

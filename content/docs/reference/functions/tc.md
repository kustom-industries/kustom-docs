---
title: TC - text converter
type: docs
categories:
  - Functions
  - Reference
tags:
  - Function
  - tc
  - text converter
---
<h1>TC: text converter (cut text, make it lowercase, uppercase, capitalized, use regular expressions…)</h1>

<h3>Syntax</h3>
tc(mode, text)
<h3>Arguments</h3>
<ul>
  <li><b>mode</b>: Conversion mode, l for lowercase, u for uppercase, c for capitalize</li>
  <li><b>text</b>: Text to convert</li>
</ul>

<h3>Examples</h3>
<table>
  <tr>
    <th><b>Formula</b></th>
    <th><b>Description</b></th>
  </tr>
  <tr>
    <td width="250px"><b>$tc(low, "sOme tExT")$</b></td>
    <td>Convert text to lower case</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(up, "sOme tExT")$</b></td>
    <td>Convert text to upper case</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(cap, "sOme tExT")$</b></td>
    <td>Capitalize words in text</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(cut, "sOme tExT", 4)$</b></td>
    <td>Will print only first 4 chars</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(ell, "sOme tExT", 4)$</b></td>
    <td>Will ellipsize (so cut and add &#8230;) if text is longer than 4 chars</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(cut, "sOme tExT", 2, 5)$</b></td>
    <td>Prints 5 chars starting after the second</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(cut, "sOme tExT", -2)$</b></td>
    <td>Prints last 2 chars</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(count, "To be or not to be", be)$</b></td>
    <td>Count the number of times a set of chars appears in text</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(utf, "201")$</b></td>
    <td>Will render utf code 0x201 (advanced, for font icons)</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(len, "sOme tExT")$</b></td>
    <td>Will return the length of text</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(n2w, 42)$</b></td>
    <td>Converts numbers to words</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(ord, 1)$</b></td>
    <td>Renders ordinal suffix for number 1 (st)</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(roman, "Year 476?")$</b></td>
    <td>Converts 476 into Roman numeral CDLXXVI</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(lpad, 5, 10, 0)$</b></td>
    <td>Will left pad the string to 'n' chars length using the provided string (or 0 by default)</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(rpad, 5, 10, 0)$</b></td>
    <td>Will right pad the string to 'n' chars length using the provided string (or 0 by default)</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(split, "SuperXOneXZed", "X", 1)$</b></td>
    <td>Split string by char X and prints second segment</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(reg, "Foobar one", "o+", X)$</b></td>
    <td>Will replace text matching o+ regexp with a capital X</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(html, "&lt;b&gt;Four&lt;/b&gt; is %gt; than 3")$</b></td>
    <td>Converts HTML text into plain text</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(url, "an URL parameter with 'strange!' symbols")$</b></td>
    <td>URL encode text using UTF-8 or supplied encoding</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(fmt, "Padded number '%05d' or text '%5s'", 3, foo)$</b></td>
    <td>Convert text and parameters using JAVA format standard</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(nfmt, "Total is 30000.12")$</b></td>
    <td>Converts numbers to proper locale format</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(lines, "This is&#10;two lines")$</b></td>
    <td>Return number of lines on a given text</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(json, "{'a':1,'b':2}", ".a")$</b></td>
    <td>Parse string as a JSON Path expression and return result</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(type, "&#1069;&#1090;&#1086; &#1083;&#1091;&#1095;&#1096;&#1077;&#1077; &#1087;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1077; &#1074; &#1084;&#1080;&#1088;&#1077;!")$</b></td>
    <td>Returns text type of the string, one of:: &#10;LATIN, &#10;NUMBER, &#10;ARABIC, &#10;CYRILLIC, &#10;GREEK, &#10;HIRAGANA, &#10;KATAKANA, &#10;CJK</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(asort, "zebra apple banana")$</b></td>
    <td>Sorts words alphabetically</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(asort, "cherry:apple:banana", ":", "desc")$</b></td>
    <td>Sorts words in descending alphabetical order</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(nsort, "3,1,2", ",")$</b></td>
    <td>Sorts numbers numerically</td>
  </tr>
  <tr>
    <td width="250px"><b>$tc(nsort, "afoo2:blabla3:zebra1", ":")$</b></td>
    <td>Extract numbers and sort words by extracted numbers</td>
  </tr>
</table>

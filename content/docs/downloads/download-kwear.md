---
title: kwear
type: docs
categories:
  - Downloads
tags:
  - kwear
  - apk
  - aosp
  - changelog
---
# kwear downloads


## Latest stable: 0.01
**Download**: <b><a href="https://kustom.rocks/download/kwear/1435216/google_release">Google Play APK</a></b><br/>
Build: 0.01b435216<br/>
Size: 23.91mb<br/>
Uploaded: 2025-03-04T18:02:26.092574+00:00<br/>






### Previous builds 
  - <a href="https://kustom.rocks/download/kwear/1432414/google_release">0.01b432414</a> 2025-03-04T18:03:19.729520+00:00
  - <a href="https://kustom.rocks/download/kwear/1421511/google_release">0.01b421511</a> 2025-03-04T18:05:00.949983+00:00
  - <a href="https://kustom.rocks/download/kwear/1429114/google_release">0.01b429114</a> 2025-03-04T18:04:12.496241+00:00
  - <a href="https://kustom.rocks/download/kwear/1428214/google_release">0.01b428214</a> 2025-03-04T18:04:28.458747+00:00
  - <a href="https://kustom.rocks/download/kwear/1328615/google_release">0.01b328615</a> 2025-03-04T18:09:27.306433+00:00


## No beta right now, stay tuned!


## Previous releases:

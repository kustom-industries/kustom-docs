---
title: klck
type: docs
categories:
  - Downloads
tags:
  - klck
  - apk
  - aosp
  - changelog
---
# klck downloads


## Latest stable: 3.77
**Download**: <b><a href="https://kustom.rocks/download/klck/377435215/google_release">Google Play APK</a></b><br/>
Build: 3.77b435215<br/>
Size: 29.77mb<br/>
Uploaded: 2025-03-04T16:43:37.895289+00:00<br/>

### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/377435216/huawei_release">3.77b435216huawei</a> (App Gallery APK) 






### Previous builds 
  - <a href="https://kustom.rocks/download/klck/377432414/google_release">3.77b432414</a> 2025-03-04T16:44:44.989928+00:00
  - <a href="https://kustom.rocks/download/klck/377428214/google_beta">3.77b428214beta</a> 2025-03-04T16:45:58.708208+00:00
  - <a href="https://kustom.rocks/download/klck/377434612/google_release">3.77b434610</a> 2025-03-04T16:44:09.288617+00:00
  - <a href="https://kustom.rocks/download/klck/377432016/google_release">3.77b432016</a> 2025-03-04T16:44:59.528537+00:00
  - <a href="https://kustom.rocks/download/klck/377434816/google_release">3.77b434815</a> 2025-03-04T16:43:52.137502+00:00



## Latest beta: 3.78
**Download**: <b><a href="https://kustom.rocks/download/klck/378506913/google_beta">Google Play APK</a></b><br/>
Build: 3.78b506913beta<br/>
Size: 30.66mb<br/>
Uploaded: 2025-03-10T15:23:28Z<br/>


 



### Previous builds
  - <a href="https://kustom.rocks/download/klck/378506614/google_beta">3.78b506614beta</a> 2025-03-07T15:02:35Z
  - <a href="https://kustom.rocks/download/klck/378506515/google_beta">3.78b506515beta</a> 2025-03-06T15:31:23Z
  - <a href="https://kustom.rocks/download/klck/378506516/google_beta">3.78b506516beta</a> 2025-03-06T16:55:10Z
  - <a href="https://kustom.rocks/download/klck/378506610/google_beta">3.78b506610beta</a> 2025-03-07T11:25:55Z



## Previous releases:

### v3.76<br/>
Download: <b><a href="https://kustom.rocks/download/klck/376422110/google_release">Google Play APK</a></b><br/>
Build: 3.76b422110<br/>
Size: 30.01mb<br/>
Uploaded: 2025-03-04T16:46:18.240302+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klck/376422114/aosp_release">3.76b422114aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klck/376422114/huawei_release">3.76b422114huawei</a> (App Gallery APK)




### v3.75<br/>
Download: <b><a href="https://kustom.rocks/download/klck/375410013/google_release">Google Play APK</a></b><br/>
Build: 3.75b410013<br/>
Size: 30.12mb<br/>
Uploaded: 2025-03-04T16:47:38.082819+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/375331812/huawei_release">3.75b331812huawei</a> (App Gallery APK)




### v3.74<br/>
Download: <b><a href="https://kustom.rocks/download/klck/374331712/google_release">Google Play APK</a></b><br/>
Build: 3.74b331712<br/>
Size: 27.86mb<br/>
Uploaded: 2025-03-04T16:50:10.914105+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klck/374326815/aosp_release">3.74b326815aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klck/374331812/huawei_release">3.74b331812huawei</a> (App Gallery APK)




### v3.73<br/>
Download: <b><a href="https://kustom.rocks/download/klck/373314511/google_release">Google Play APK</a></b><br/>
Build: 3.73b314511<br/>
Size: 27.25mb<br/>
Uploaded: 2025-03-04T16:55:57.480294+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>Added new free path in Shapes, draw ANYTHING you want! Check out https://kustom.rocks/svgpath</li><li>Added confirm dialog on preset delete</li><li>You can now spell numbers and clock in Japanese</li><li>You can now parse dates in ISO or other formats via dp()</li><li>Hiding duplicates in font picker</li><li>Fixed issues with preset import on file open</li><li>Fixed sizing issues on foldable screens</li><li>Fixed flow issue not hiding when closing bottom menu</li><li>Fixed gradients in squircle shape</li><li>Fix KLWP disappearing objects when close to the edge</li><li>Fix issue on Samsung devices when adding a shortcut</li><li>Fix Kustom forgets Tasker vars not updated for more than 6 days</li></ul>

### v3.72<br/>
Download: <b><a href="https://kustom.rocks/download/klck/372310707/google_release">Google Play APK</a></b><br/>
Build: 3.72b310707<br/>
Size: 27.89mb<br/>
Uploaded: 2025-03-04T16:56:57.340630+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klck/372310707/aosp_release">3.72b310707aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>You can now set globals using an URI like kwgt://global/name/value</li><li>Fixed new presets not shown after export or import</li><li>Fixes lag on KLWP in some preset</li></ul>

### v3.71<br/>
Download: <b><a href="https://kustom.rocks/download/klck/371308214/google_release">Google Play APK</a></b><br/>
Build: 3.71b308214<br/>
Size: 27.90mb<br/>
Uploaded: 2025-03-04T16:57:42.856339+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klck/371308215/aosp_release">3.71b308215aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>You can now C style use /* comments */ inside $kustom expressions$</li><li>Brand new export dialog, feedback welcome!</li><li>You can now export as image</li><li>Flows can open URI and Intent links (so you can set Tasker vars)</li><li>Flows can store any type of globals not just text globals</li><li>Fixes new preset being re-exported causing duplicates</li><li>Fixes music player info providing wrong package name</li><li>Fixes feels like temperature not using provider data in AccuWeather</li><li>Fixed "$wi(tempu)$" not changing when settings changed</li></ul>

### v3.70<br/>
Download: <b><a href="https://kustom.rocks/download/klck/370303210/google_release">Google Play APK</a></b><br/>
Build: 3.70b303210<br/>
Size: 25.61mb<br/>
Uploaded: 2025-03-04T16:59:14.362688+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/370303210/huawei_release">3.70b303210huawei</a> (App Gallery APK)
  - **aosp**: <a href="https://kustom.rocks/download/klck/370303212/aosp_release">3.70b303212aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>Added Kustom Flows to bring you where no one has gone before https://kustom.rocks/flows</li><li>Added Secret Globals to store sensitive data like API keys in locked komponents</li><li>Added support for remote messages (http post from the internet), see https://kustom.rocks/remotemsg</li><li>Added support for Material You "Monet" accent and neutral colors via $si(sysca1/a2/a3/n1/n2, shadelevel)$ function https://kustom.rocks/dyncolors</li><li>KLWP now detects if its zoomed via $si(wgzoomed)$, can be used on some launcher to understand when drawer or recents are opened</li><li>Improved speed of listing entries / fonts from SD</li><li>Fixed KWGT not correctly updating when a global was changed</li><li>Fixed YRNO weather provider current conditions not always displayed</li></ul>

### v3.63<br/>
Download: <b><a href="https://kustom.rocks/download/klck/363228708/google_release">Google Play APK</a></b><br/>
Build: 3.63b228708<br/>
Size: 22.78mb<br/>
Uploaded: 2025-03-04T17:04:47.708204+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/363228708/huawei_release">3.63b228708huawei</a> (App Gallery APK)
  - **aosp**: <a href="https://kustom.rocks/download/klck/363228709/aosp_release">3.63b228709aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>New lv() function can set local variables</li><li>In KWGT si(darkmode) now only returns system dark mode</li><li>Added si(darkwp) that returns 1 when wallpaper colors prefer a dark theme</li><li>Formula faves are now also stored on external storage as a backup measure</li></ul>

### v3.62<br/>
Download: <b><a href="https://kustom.rocks/download/klck/362224415/google_release">Google Play APK</a></b><br/>
Build: 3.62b224415<br/>
Size: 22.36mb<br/>
Uploaded: 2025-03-04T17:05:40.039522+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/362224416/huawei_release">3.62b224416huawei</a> (App Gallery APK)
  - **aosp**: <a href="https://kustom.rocks/download/klck/362224417/aosp_release">3.62b224417aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>Font picker now shows current text as preview</li><li>Font picker UI improvements</li><li>Fixed duplicate issue on exporting some preset</li><li>Fixes notifications export issues</li><li>Fixes random image and file not working even in Kustom subfolders</li></ul>

### v3.61<br/>
Download: <b><a href="https://kustom.rocks/download/klck/361223012/google_release">Google Play APK</a></b><br/>
Build: 3.61b223012<br/>
Size: 22.43mb<br/>
Uploaded: 2025-03-04T17:06:13.752101+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/361223013/huawei_release">3.61b223013huawei</a> (App Gallery APK)
  - **aosp**: <a href="https://kustom.rocks/download/klck/361223014/aosp_release">3.61b223014aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>Improved save speed for presets with lots of resources</li><li>Slightly improved external font / font packs loading speed</li><li>Fixed KLWP 5 secs delay</li><li>Fixed font picker not remembering scroll position</li><li>Fixed alpha ordering in font picker</li><li>Fixed recent/faves not working for local storage presets</li></ul>

### v3.60<br/>
Download: <b><a href="https://kustom.rocks/download/klck/360220710/google_release">Google Play APK</a></b><br/>
Build: 3.60b220710<br/>
Size: 22.41mb<br/>
Uploaded: 2025-03-04T17:07:35.900144+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/360220710/huawei_release">3.60b220710huawei</a> (App Gallery APK)
  - **aosp**: <a href="https://kustom.rocks/download/klck/360220712/aosp_release">3.60b220712aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>Fixed local fonts not saved</li></ul>

### v3.59<br/>
Download: <b><a href="https://kustom.rocks/download/klck/359220615/google_release">Google Play APK</a></b><br/>
Build: 3.59b220615<br/>
Size: 22.41mb<br/>
Uploaded: 2025-03-04T17:07:50.013370+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klck/359220616/aosp_release">3.59b220616aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klck/359220616/huawei_release">3.59b220616huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Hot fix exporting presets will not store resources</li></ul>

### v3.58<br/>
Download: <b><a href="https://kustom.rocks/download/klck/358217313/google_release">Google Play APK</a></b><br/>
Build: 3.58b217313<br/>
Size: 22.41mb<br/>
Uploaded: 2025-03-04T17:08:34.158179+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/358217314/huawei_release">3.58b217314huawei</a> (App Gallery APK)
  - **aosp**: <a href="https://kustom.rocks/download/klck/358217315/aosp_release">3.58b217315aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>Removed access to SD Card due to new security policies, storage migration will be requested</li><li>New font picker now shows Google fonts automatically</li><li>KLWP workaround for Samsung Android 12 bug</li><li>KLWP ignores wallpaper color for dark mode</li><li>You can purchase KWGT without separate pro key</li><li>A big thanks to all the users keeping translations up to date! You rock!</li></ul>

### v3.57<br/>
Download: <b><a href="https://kustom.rocks/download/klck/357121813/google_release">Google Play APK</a></b><br/>
Build: 3.57b121813<br/>
Size: 19.13mb<br/>
Uploaded: 2025-03-04T17:12:59.127246+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klck/357121814/aosp_release">3.57b121814aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klck/357121814/huawei_release">3.57b121814huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Packs are now correctly showing newer items first in loader</li><li>Added Projekt launcher support on KLWP</li><li>Removed Yahoo weather :(</li></ul>

### v3.56<br/>
Download: <b><a href="https://kustom.rocks/download/klck/356114416/google_release">Google Play APK</a></b><br/>
Build: 3.56b114416<br/>
Size: 19.06mb<br/>
Uploaded: 2025-03-04T17:13:22.384097+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/356114416/huawei_release">3.56b114416huawei</a> (App Gallery APK)
  - **aosp**: <a href="https://kustom.rocks/download/klck/356114417/aosp_release">3.56b114417aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>Fixes color picker not showing recent colors</li><li>Fixes load preset tasker action</li><li>Fixes load preset from file explorer / telegram</li><li>Fixes color picker unusable on small screens</li></ul>

### v3.55<br/>
Download: <b><a href="https://kustom.rocks/download/klck/355112308/google_release">Google Play APK</a></b><br/>
Build: 3.55b112308<br/>
Size: 19.08mb<br/>
Uploaded: 2025-03-04T17:13:33.318855+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/355112309/huawei_release">3.55b112309huawei</a> (App Gallery APK)
  - **aosp**: <a href="https://kustom.rocks/download/klck/355112313/aosp_release">3.55b112313aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>New color picker, feedback welcome!</li><li>Added support for global folders</li><li>Fix issues with TF and settings on some device</li><li>Fix empty filter locks you in the loader</li></ul>

### v3.54<br/>
Download: <b><a href="https://kustom.rocks/download/klck/354106810/google_release">Google Play APK</a></b><br/>
Build: 3.54b106810<br/>
Size: 18.77mb<br/>
Uploaded: 2025-03-04T17:14:25.785447+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/354106810/huawei_release">3.54b106810huawei</a> (App Gallery APK)
  - **aosp**: <a href="https://kustom.rocks/download/klck/354107408/aosp_release">3.54b107408aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>Fix loading directly from packs dashboard not working</li><li>Fix issues with location search</li><li>Language updates</li></ul>

### v3.53<br/>
Download: <b><a href="https://kustom.rocks/download/klck/353104015/google_release">Google Play APK</a></b><br/>
Build: 3.53b104015<br/>
Size: 18.57mb<br/>
Uploaded: 2025-03-04T17:14:37.153452+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/353104015/huawei_release">3.53b104015huawei</a> (App Gallery APK)
  - **aosp**: <a href="https://kustom.rocks/download/klck/353104107/aosp_release">3.53b104107aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>New purchase dialog, feedback welcome</li><li>You can now browse installed packs in the main loader</li><li>Fix YrNo will stop working 31st of March</li><li>Fix KLWP sometimes launching wrong app</li><li>Fix pressing edit in the widget will then make load preset fail</li><li>Fix issues on Samsung when in Airplane Mode</li><li>Fix preview scaling in KLWP loader</li><li>Fix hardcoded max at 720 in number globals</li></ul>

### v3.52<br/>
Download: <b><a href="https://kustom.rocks/download/klck/352102617/google_release">Google Play APK</a></b><br/>
Build: 3.52b102617<br/>
Size: 18.49mb<br/>
Uploaded: 2025-03-04T17:15:35.088253+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/352102618/huawei_release">3.52b102618huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>New loader window, feedback welcome!</li><li>Added tc(url) to encode URLs</li><li>Added si(powersave) to show when power save is on</li><li>Fixed traditional and simplified Chinese in language settings</li><li>Fixed delay to launch in Android 11 for KLWP</li><li>Fixed location search not working in some language</li><li>Fixed crop not working in the editor</li><li>Fixed BT state not updated on change</li><li>Fixed SIM count on some device</li></ul>

### v3.51<br/>
Download: <b><a href="https://kustom.rocks/download/klck/351030911/google_release">Google Play APK</a></b><br/>
Build: 3.51b30911<br/>
Size: 18.13mb<br/>
Uploaded: 2025-03-04T17:18:09.930772+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/351030911/huawei_release">3.51b30911huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>New intro and settings</li><li>New KLWP control refresh rate in the advanced settings</li><li>Fixes Chinese language cannot be forced</li><li>Fixed some music players not being recognized</li><li>Fixes slow downs / battery issues when using palette formulas</li><li>Fixes battery duration / last plugged issues on Android 11</li></ul>

### v3.50<br/>
Download: <b><a href="https://kustom.rocks/download/klck/350028512/google_release">Google Play APK</a></b><br/>
Build: 3.50b28512<br/>
Size: 12.98mb<br/>
Uploaded: 2025-03-04T17:20:44.879593+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/350028512/huawei_release">3.50b28512</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>HOTFIX fonts and icon fonts picker not working</li></ul>

### v3.49<br/>
Download: <b><a href="https://kustom.rocks/download/klck/349027509/google_release">Google Play APK</a></b><br/>
Build: 3.49b27509<br/>
Size: 12.97mb<br/>
Uploaded: 2025-03-04T17:20:53.429315+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klck/349027509/huawei_release">3.49b27509</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Added support for battery level of BT devices (Android 8 or newer)</li><li>Added support for 5G network type</li><li>Added squircle shape so you can get that IOS picture widget :)</li><li>Slightly faster export / import</li><li>Removed toggle WiFi and BT due to API 29 upgrade, sorry, blame Google</li><li>KLWP has a new option to handle raw touch on launchers where touch does not work</li><li>You can copy and paste globals</li><li>Fixed TS unit wrong</li><li>Fixed TU not working with negative values</li><li>KLCK Fixed support for Android 10</li><li>KLCK Fixed lock not displaying on Android 8 or newer without system lock</li><li>KLCK Fixed music visualizer</li></ul>

### v3.48<br/>
Download: <b><a href="https://kustom.rocks/download/klck/348021016/google_release">Google Play APK</a></b><br/>
Build: 3.48b21016<br/>
Size: 12.67mb<br/>
Uploaded: 2025-03-04T17:22:50.204284+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>KLWP Added support for music visualization</li><li>KLWP General availability of Movie Module</li><li>Added rounded corners to triangles, exagons</li><li>Added CJK for Chinese chars in tc(type)</li><li>Weather plugin now supports also WeatherBit and, for Australia, WillyWeather</li><li>Fixed force weather update touch action not working</li><li>Fixed download/upload speed showing wrong values realtime</li><li>Fixed animation formulas not showing formula value</li></ul>

### v3.47<br/>
Download: <b><a href="https://kustom.rocks/download/klck/347016716/google_release">Google Play APK</a></b><br/>
Build: 3.47b16716<br/>
Size: 12.90mb<br/>
Uploaded: 2025-03-04T17:43:14.258768+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>New you can select multiple preferred music players in the settings</li><li>Increased max loops in for loops</li><li>Fix preferred music player not forced when other players detected</li><li>Fix YT Music and Google Podcasts not being detected as players</li></ul>

### v3.46<br/>
Download: <b><a href="https://kustom.rocks/download/klck/346014609/google_release">Google Play APK</a></b><br/>
Build: 3.46b14609<br/>
Size: 12.77mb<br/>
Uploaded: 2025-03-04T17:43:41.850912+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>New scale modes in Bitmap Module (fit height, center fit and center crop)</li><li>New text module fit box mode to control height and width</li><li>New hex and decimal conversion in mu()</li><li>New count text occurences with tc(count)</li><li>Fix palette never returning black as a color</li><li>Fix in current not working on some device</li><li>Fix ce(contrast) returning black too often</li><li>Fix forcing Chinese language not working</li><li>KLWP Fix visibility issues during animations</li></ul>

### v3.45<br/>
Download: <b><a href="https://kustom.rocks/download/klck/345007815/google_release">Google Play APK</a></b><br/>
Build: 3.45b7815<br/>
Size: 12.72mb<br/>
Uploaded: 2025-03-04T17:44:12.841734+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>Higher max res for bitmaps (should fix blurry images on some preset)</li><li>Fixed issues in KWGT with signal/wifi toggles</li><li>Fixed plugins issues with KWGT (broadcast)</li><li>Fixed issues with multiple widgets using different image backgrounds</li><li>Fixes issues with toggles / formulas not switching on / off</li><li>Fixes battery current on Samsung devices</li><li>Added traditional Chinese to the language options</li></ul>

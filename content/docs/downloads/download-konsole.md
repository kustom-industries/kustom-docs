---
title: konsole
type: docs
categories:
  - Downloads
tags:
  - konsole
  - apk
  - aosp
  - changelog
---
# konsole downloads


## Latest stable: 1.31
**Download**: <b><a href="https://kustom.rocks/download/konsole/131/google_release">Google Play APK</a></b><br/>
Build: 1.31<br/>
Size: 19.82mb<br/>
Uploaded: 2025-03-04T18:12:31.466960+00:00<br/>






## No beta right now, stay tuned!


## Previous releases:

### v1.30<br/>
Download: <b><a href="https://kustom.rocks/download/konsole/130/google_release">Google Play APK</a></b><br/>
Build: 1.30<br/>
Size: 19.82mb<br/>
Uploaded: 2025-03-04T18:12:42.661771+00:00<br/>



### v1.20<br/>
Download: <b><a href="https://kustom.rocks/download/konsole/120/google_release">Google Play APK</a></b><br/>
Build: 1.20<br/>
Size: 18.68mb<br/>
Uploaded: 2025-03-04T18:12:54.605152+00:00<br/>



### v1.18<br/>
Download: <b><a href="https://kustom.rocks/download/konsole/118/google_release">Google Play APK</a></b><br/>
Build: 1.18<br/>
Size: 18.68mb<br/>
Uploaded: 2025-03-04T18:13:05.468609+00:00<br/>



### v1.14<br/>
Download: <b><a href="https://kustom.rocks/download/konsole/114/google_release">Google Play APK</a></b><br/>
Build: 1.14<br/>
Size: 18.68mb<br/>
Uploaded: 2025-03-04T18:13:15.828556+00:00<br/>



### v1.10<br/>
Download: <b><a href="https://kustom.rocks/download/konsole/109/google_release">Google Play APK</a></b><br/>
Build: 1.10<br/>
Size: 14.60mb<br/>
Uploaded: 2025-03-04T18:13:25.188658+00:00<br/>



### v1.05<br/>
Download: <b><a href="https://kustom.rocks/download/konsole/105/google_release">Google Play APK</a></b><br/>
Build: 1.05<br/>
Size: 14.57mb<br/>
Uploaded: 2025-03-04T18:13:36.153149+00:00<br/>



### v1.03<br/>
Download: <b><a href="https://kustom.rocks/download/konsole/103/google_release">Google Play APK</a></b><br/>
Build: 1.03<br/>
Size: 14.57mb<br/>
Uploaded: 2025-03-04T18:13:42.244404+00:00<br/>



### v1.02<br/>
Download: <b><a href="https://kustom.rocks/download/konsole/102/google_release">Google Play APK</a></b><br/>
Build: 1.02<br/>
Size: 14.57mb<br/>
Uploaded: 2025-03-04T18:13:56.195699+00:00<br/>



### v1.01<br/>
Download: <b><a href="https://kustom.rocks/download/konsole/101/google_release">Google Play APK</a></b><br/>
Build: 1.01<br/>
Size: 14.57mb<br/>
Uploaded: 2025-03-04T18:14:12.348841+00:00<br/>



### v1.0<br/>
Download: <b><a href="https://kustom.rocks/download/konsole/1/google_release">Google Play APK</a></b><br/>
Build: 1.0<br/>
Size: 14.57mb<br/>
Uploaded: 2025-03-04T18:14:25.448468+00:00<br/>



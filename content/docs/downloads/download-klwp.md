---
title: klwp
type: docs
categories:
  - Downloads
tags:
  - klwp
  - apk
  - aosp
  - changelog
---
# klwp downloads


## Latest stable: 3.77
**Download**: <b><a href="https://kustom.rocks/download/klwp/377435216/google_release">Google Play APK</a></b><br/>
Build: 3.77b435216<br/>
Size: 31.11mb<br/>
Uploaded: 2025-03-04T15:13:10.583789+00:00<br/>

### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klwp/377435216/huawei_release">3.77b435216huawei</a> (App Gallery APK) 






### Previous builds 
  - <a href="https://kustom.rocks/download/klwp/377428214/google_beta">3.77b428214beta</a> 2025-03-04T15:15:40.590249+00:00
  - <a href="https://kustom.rocks/download/klwp/377429114/google_beta">3.77b429114beta</a> 2025-03-04T15:15:23.852129+00:00
  - <a href="https://kustom.rocks/download/klwp/377434313/google_release">3.77b434313</a> 2025-03-04T15:13:56.849762+00:00
  - <a href="https://kustom.rocks/download/klwp/377429510/google_beta">3.77b429510beta</a> 2025-03-04T15:15:05.701926+00:00
  - <a href="https://kustom.rocks/download/klwp/377434612/google_release">3.77b434610</a> 2025-03-04T15:13:43.960748+00:00



## Latest beta: 3.78
**Download**: <b><a href="https://kustom.rocks/download/klwp/378506913/google_beta">Google Play APK</a></b><br/>
Build: 3.78b506913beta<br/>
Size: 32.07mb<br/>
Uploaded: 2025-03-10T15:22:53Z<br/>


 



### Previous builds
  - <a href="https://kustom.rocks/download/klwp/378506614/google_beta">3.78b506614beta</a> 2025-03-07T15:02:06Z
  - <a href="https://kustom.rocks/download/klwp/378506516/google_beta">3.78b506516beta</a> 2025-03-06T16:55:10Z
  - <a href="https://kustom.rocks/download/klwp/378506611/google_beta">3.78b506611beta</a> 2025-03-07T11:26:14Z
  - <a href="https://kustom.rocks/download/klwp/378506515/google_beta">3.78b506515beta</a> 2025-03-06T15:35:00Z



## Previous releases:

### v3.76<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/376422110/google_release">Google Play APK</a></b><br/>
Build: 3.76b422110<br/>
Size: 31.36mb<br/>
Uploaded: 2025-03-04T15:15:58.332312+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/376422115/aosp_release">3.76b422115aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/376422114/huawei_release">3.76b422114huawei</a> (App Gallery APK)




### v3.75<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/375410013/google_release">Google Play APK</a></b><br/>
Build: 3.75b410013<br/>
Size: 31.53mb<br/>
Uploaded: 2025-03-04T15:17:03.071534+00:00<br/>



### v3.74<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/374331712/google_release">Google Play APK</a></b><br/>
Build: 3.74b331712<br/>
Size: 29.32mb<br/>
Uploaded: 2025-03-04T15:19:46.310972+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/374326816/aosp_release">3.74b326816aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/374331812/huawei_release">3.74b331812huawei</a> (App Gallery APK)




### v3.73<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/373314511/google_release">Google Play APK</a></b><br/>
Build: 3.73b314511<br/>
Size: 28.85mb<br/>
Uploaded: 2025-03-04T15:24:15.008521+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>Added new free path in Shapes, draw ANYTHING you want! Check out https://kustom.rocks/svgpath</li><li>Added confirm dialog on preset delete</li><li>You can now spell numbers and clock in Japanese</li><li>You can now parse dates in ISO or other formats via dp()</li><li>Hiding duplicates in font picker</li><li>Fixed issues with preset import on file open</li><li>Fixed sizing issues on foldable screens</li><li>Fixed flow issue not hiding when closing bottom menu</li><li>Fixed gradients in squircle shape</li><li>Fix KLWP disappearing objects when close to the edge</li><li>Fix issue on Samsung devices when adding a shortcut</li><li>Fix Kustom forgets Tasker vars not updated for more than 6 days</li></ul>

### v3.72<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/372311310/google_release">Google Play APK</a></b><br/>
Build: 3.72b311310<br/>
Size: 29.39mb<br/>
Uploaded: 2025-03-04T15:25:16.588861+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/372310707/aosp_release">3.72b310707aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>You can now set globals using an URI like kwgt://global/name/value</li><li>Fixed new presets not shown after export or import</li><li>Fixes lag on KLWP in some preset</li></ul>

### v3.71<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/371308214/google_release">Google Play APK</a></b><br/>
Build: 3.71b308214<br/>
Size: 29.37mb<br/>
Uploaded: 2025-03-04T15:26:02.865414+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/371308215/aosp_release">3.71b308215aosp</a> (No PRO required, ADS supported, no Google Services)


Release Notes:
<ul style="margin: 0"><li>You can now C style use /* comments */ inside $kustom expressions$</li><li>Brand new export dialog, feedback welcome!</li><li>You can now export as image</li><li>Flows can open URI and Intent links (so you can set Tasker vars)</li><li>Flows can store any type of globals not just text globals</li><li>Fixes new preset being re-exported causing duplicates</li><li>Fixes music player info providing wrong package name</li><li>Fixes feels like temperature not using provider data in AccuWeather</li><li>Fixed "$wi(tempu)$" not changing when settings changed</li></ul>

### v3.70<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/370303210/google_release">Google Play APK</a></b><br/>
Build: 3.70b303210<br/>
Size: 27.11mb<br/>
Uploaded: 2025-03-04T15:27:37.600432+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/370303212/aosp_release">3.70b303212aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/370303210/huawei_release">3.70b303210huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Added Kustom Flows to bring you where no one has gone before https://kustom.rocks/flows</li><li>Added Secret Globals to store sensitive data like API keys in locked komponents</li><li>Added support for remote messages (http post from the internet), see https://kustom.rocks/remotemsg</li><li>Added support for Material You "Monet" accent and neutral colors via $si(sysca1/a2/a3/n1/n2, shadelevel)$ function https://kustom.rocks/dyncolors</li><li>KLWP now detects if its zoomed via $si(wgzoomed)$, can be used on some launcher to understand when drawer or recents are opened</li><li>Improved speed of listing entries / fonts from SD</li><li>Fixed KWGT not correctly updating when a global was changed</li><li>Fixed YRNO weather provider current conditions not always displayed</li></ul>

### v3.63<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/363228708/google_release">Google Play APK</a></b><br/>
Build: 3.63b228708<br/>
Size: 24.22mb<br/>
Uploaded: 2025-03-04T15:33:32.769211+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/363228709/aosp_release">3.63b228709aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/363228708/huawei_release">3.63b228708huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>New lv() function can set local variables</li><li>In KWGT si(darkmode) now only returns system dark mode</li><li>Added si(darkwp) that returns 1 when wallpaper colors prefer a dark theme</li><li>Formula faves are now also stored on external storage as a backup measure</li></ul>

### v3.62<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/362224415/google_release">Google Play APK</a></b><br/>
Build: 3.62b224415<br/>
Size: 23.82mb<br/>
Uploaded: 2025-03-04T15:34:24.954637+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/362224417/aosp_release">3.62b224417aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/362224416/huawei_release">3.62b224416huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Font picker now shows current text as preview</li><li>Font picker UI improvements</li><li>Fixed duplicate issue on exporting some preset</li><li>Fixes notifications export issues</li><li>Fixes random image and file not working even in Kustom subfolders</li></ul>

### v3.61<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/361223012/google_release">Google Play APK</a></b><br/>
Build: 3.61b223012<br/>
Size: 23.81mb<br/>
Uploaded: 2025-03-04T15:35:02.706600+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/361223014/aosp_release">3.61b223014aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/361223013/huawei_release">3.61b223013huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Improved save speed for presets with lots of resources</li><li>Slightly improved external font / font packs loading speed</li><li>Fixed KLWP 5 secs delay</li><li>Fixed font picker not remembering scroll position</li><li>Fixed alpha ordering in font picker</li><li>Fixed recent/faves not working for local storage presets</li></ul>

### v3.60<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/360220710/google_release">Google Play APK</a></b><br/>
Build: 3.60b220710<br/>
Size: 23.79mb<br/>
Uploaded: 2025-03-04T15:36:40.089510+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/360220712/aosp_release">3.60b220712aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/360220710/huawei_release">3.60b220710huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Fixed local fonts not saved</li></ul>

### v3.59<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/359220615/google_release">Google Play APK</a></b><br/>
Build: 3.59b220615<br/>
Size: 23.79mb<br/>
Uploaded: 2025-03-04T15:36:52.526590+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/359220616/aosp_release">3.59b220616aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/359220616/huawei_release">3.59b220616huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Hot fix exporting presets will not store resources</li></ul>

### v3.58<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/358217313/google_release">Google Play APK</a></b><br/>
Build: 3.58b217313<br/>
Size: 23.79mb<br/>
Uploaded: 2025-03-04T15:37:48.118277+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/358217314/aosp_release">3.58b217314aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/358217314/huawei_release">3.58b217314huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Removed access to SD Card due to new security policies, storage migration will be requested</li><li>New font picker now shows Google fonts automatically</li><li>KLWP workaround for Samsung Android 12 bug</li><li>KLWP ignores wallpaper color for dark mode</li><li>You can purchase KWGT without separate pro key</li><li>A big thanks to all the users keeping translations up to date! You rock!</li></ul>

### v3.57<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/357121813/google_release">Google Play APK</a></b><br/>
Build: 3.57b121813<br/>
Size: 20.57mb<br/>
Uploaded: 2025-03-04T15:42:39.088571+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/357121814/aosp_release">3.57b121814aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/357121814/huawei_release">3.57b121814huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Packs are now correctly showing newer items first in loader</li><li>Added Projekt launcher support on KLWP</li><li>Removed Yahoo weather :(</li></ul>

### v3.56<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/356114416/google_release">Google Play APK</a></b><br/>
Build: 3.56b114416<br/>
Size: 20.47mb<br/>
Uploaded: 2025-03-04T15:43:05.385449+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/356114416/aosp_release">3.56b114416aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/356114416/huawei_release">3.56b114416huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Fixes color picker not showing recent colors</li><li>Fixes load preset tasker action</li><li>Fixes load preset from file explorer / telegram</li><li>Fixes color picker unusable on small screens</li></ul>

### v3.55<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/355112308/google_release">Google Play APK</a></b><br/>
Build: 3.55b112308<br/>
Size: 20.52mb<br/>
Uploaded: 2025-03-04T15:43:32.065348+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/355112313/aosp_release">3.55b112313aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/355113212/huawei_release">3.55b113212huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>New color picker, feedback welcome!</li><li>Added support for global folders</li><li>Fix issues with TF and settings on some device</li><li>Fix empty filter locks you in the loader</li></ul>

### v3.54<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/354106810/google_release">Google Play APK</a></b><br/>
Build: 3.54b106810<br/>
Size: 20.16mb<br/>
Uploaded: 2025-03-04T15:44:35.989133+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/354107408/aosp_release">3.54b107408aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/354106810/huawei_release">3.54b106810huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Fix loading directly from packs dashboard not working</li><li>Fix issues with location search</li><li>Language updates</li></ul>

### v3.53<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/353104015/google_release">Google Play APK</a></b><br/>
Build: 3.53b104015<br/>
Size: 19.93mb<br/>
Uploaded: 2025-03-04T15:44:49.106322+00:00<br/>
### Variants
  - **aosp**: <a href="https://kustom.rocks/download/klwp/353104107/aosp_release">3.53b104107aosp</a> (No PRO required, ADS supported, no Google Services)
  - **huawei**: <a href="https://kustom.rocks/download/klwp/353104015/huawei_release">3.53b104015huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>New purchase dialog, feedback welcome</li><li>You can now browse installed packs in the main loader</li><li>Fix YrNo will stop working 31st of March</li><li>Fix KLWP sometimes launching wrong app</li><li>Fix pressing edit in the widget will then make load preset fail</li><li>Fix issues on Samsung when in Airplane Mode</li><li>Fix preview scaling in KLWP loader</li><li>Fix hardcoded max at 720 in number globals</li></ul>

### v3.52<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/352102617/google_release">Google Play APK</a></b><br/>
Build: 3.52b102617<br/>
Size: 19.91mb<br/>
Uploaded: 2025-03-04T15:45:44.282623+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klwp/352102618/huawei_release">3.52b102618huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>New loader window, feedback welcome!</li><li>Added tc(url) to encode URLs</li><li>Added si(powersave) to show when power save is on</li><li>Fixed traditional and simplified Chinese in language settings</li><li>Fixed delay to launch in Android 11 for KLWP</li><li>Fixed location search not working in some language</li><li>Fixed crop not working in the editor</li><li>Fixed BT state not updated on change</li><li>Fixed SIM count on some device</li></ul>

### v3.51<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/351030911/google_release">Google Play APK</a></b><br/>
Build: 3.51b30911<br/>
Size: 19.64mb<br/>
Uploaded: 2025-03-04T15:49:00.332758+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klwp/351030911/huawei_release">3.51b30911huawei</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>New intro and settings</li><li>New KLWP control refresh rate in the advanced settings</li><li>Fixes Chinese language cannot be forced</li><li>Fixed some music players not being recognized</li><li>Fixes slow downs / battery issues when using palette formulas</li><li>Fixes battery duration / last plugged issues on Android 11</li></ul>

### v3.50<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/350028512/google_release">Google Play APK</a></b><br/>
Build: 3.50b28512<br/>
Size: 14.34mb<br/>
Uploaded: 2025-03-04T15:51:40.905560+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klwp/350028512/huawei_release">3.50b28512</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>HOTFIX fonts and icon fonts picker not working</li></ul>

### v3.49<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/349027509/google_release">Google Play APK</a></b><br/>
Build: 3.49b27509<br/>
Size: 14.34mb<br/>
Uploaded: 2025-03-04T15:51:49.253641+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klwp/349027509/huawei_release">3.49b27509</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>Added support for battery level of BT devices (Android 8 or newer)</li><li>Added support for 5G network type</li><li>Added squircle shape so you can get that IOS picture widget :)</li><li>Slightly faster export / import</li><li>Removed toggle WiFi and BT due to API 29 upgrade, sorry, blame Google</li><li>KLWP has a new option to handle raw touch on launchers where touch does not work</li><li>You can copy and paste globals</li><li>Fixed TS unit wrong</li><li>Fixed TU not working with negative values</li><li>KLCK Fixed support for Android 10</li><li>KLCK Fixed lock not displaying on Android 8 or newer without system lock</li><li>KLCK Fixed music visualizer</li></ul>

### v3.48<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/348021017/google_release">Google Play APK</a></b><br/>
Build: 3.48b21017<br/>
Size: 14.12mb<br/>
Uploaded: 2025-03-04T15:53:57.010164+00:00<br/>
### Variants
  - **huawei**: <a href="https://kustom.rocks/download/klwp/348021016/huawei_release">3.48b21016</a> (App Gallery APK)


Release Notes:
<ul style="margin: 0"><li>KLWP Added support for music visualization</li><li>KLWP General availability of Movie Module</li><li>Added rounded corners to triangles, exagons</li><li>Added CJK for Chinese chars in tc(type)</li><li>Weather plugin now supports also WeatherBit and, for Australia, WillyWeather</li><li>Fixed force weather update touch action not working</li><li>Fixed download/upload speed showing wrong values realtime</li><li>Fixed animation formulas not showing formula value</li></ul>

### v3.47<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/347016716/google_release">Google Play APK</a></b><br/>
Build: 3.47b16716<br/>
Size: 14.33mb<br/>
Uploaded: 2025-03-04T15:55:24.616935+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>New you can select multiple preferred music players in the settings</li><li>Increased max loops in for loops</li><li>Fix preferred music player not forced when other players detected</li><li>Fix YT Music and Google Podcasts not being detected as players</li></ul>

### v3.46<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/346014609/google_release">Google Play APK</a></b><br/>
Build: 3.46b14609<br/>
Size: 14.14mb<br/>
Uploaded: 2025-03-04T15:55:59.674570+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>New scale modes in Bitmap Module (fit height, center fit and center crop)</li><li>New text module fit box mode to control height and width</li><li>New hex and decimal conversion in mu()</li><li>New count text occurences with tc(count)</li><li>Fix palette never returning black as a color</li><li>Fix in current not working on some device</li><li>Fix ce(contrast) returning black too often</li><li>Fix forcing Chinese language not working</li><li>KLWP Fix visibility issues during animations</li></ul>

### v3.45<br/>
Download: <b><a href="https://kustom.rocks/download/klwp/345007815/google_release">Google Play APK</a></b><br/>
Build: 3.45b7815<br/>
Size: 14.09mb<br/>
Uploaded: 2025-03-04T16:17:18.508331+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>Higher max res for bitmaps (should fix blurry images on some preset)</li><li>Fixed issues in KWGT with signal/wifi toggles</li><li>Fixed plugins issues with KWGT (broadcast)</li><li>Fixed issues with multiple widgets using different image backgrounds</li><li>Fixes issues with toggles / formulas not switching on / off</li><li>Fixes battery current on Samsung devices</li><li>Added traditional Chinese to the language options</li></ul>

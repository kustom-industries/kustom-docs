---
title: kunread
type: docs
categories:
  - Downloads
tags:
  - kunread
  - apk
  - aosp
  - changelog
---
# kunread downloads


## Latest stable: 1.1
**Download**: <b><a href="https://kustom.rocks/download/kunread/2/google_release">Google Play APK</a></b><br/>
Build: 1.1<br/>
Size: 1.68mb<br/>
Uploaded: 2025-03-10T13:17:11.887157+00:00<br/>






## No beta right now, stay tuned!


## Previous releases:

### v1.0<br/>
Download: <b><a href="https://kustom.rocks/download/kunread/1/google_release">Google Play APK</a></b><br/>
Build: 1.0<br/>
Size: 1.30mb<br/>
Uploaded: 2025-03-10T13:17:16.186923+00:00<br/>



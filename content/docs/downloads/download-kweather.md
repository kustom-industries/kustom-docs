---
title: kweather
type: docs
categories:
  - Downloads
tags:
  - kweather
  - apk
  - aosp
  - changelog
---
# kweather downloads


## Latest stable: 1.30
**Download**: <b><a href="https://kustom.rocks/download/kweather/130416606/google_release">Google Play APK</a></b><br/>
Build: 1.30b416606<br/>
Size: 12.88mb<br/>
Uploaded: 2025-03-04T18:11:18.558427+00:00<br/>






## No beta right now, stay tuned!


## Previous releases:

### v1.21<br/>
Download: <b><a href="https://kustom.rocks/download/kweather/121308209/google_release">Google Play APK</a></b><br/>
Build: 1.21b308209<br/>
Size: 11.12mb<br/>
Uploaded: 2025-03-04T18:11:27.040878+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>Fixes crash in querying details</li></ul>

### v1.20<br/>
Download: <b><a href="https://kustom.rocks/download/kweather/120022811/google_release">Google Play APK</a></b><br/>
Build: 1.20b22811<br/>
Size: 5.17mb<br/>
Uploaded: 2025-03-04T18:11:31.980141+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>Fixes crash in querying details</li></ul>

### v1.12<br/>
Download: <b><a href="https://kustom.rocks/download/kweather/112921910/google_release">Google Play APK</a></b><br/>
Build: 1.12b921910<br/>
Size: 3.61mb<br/>
Uploaded: 2025-03-04T18:11:37.274415+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>Fixes crash in querying details</li></ul>

### v1.11<br/>
Download: <b><a href="https://kustom.rocks/download/kweather/111919904/google_release">Google Play APK</a></b><br/>
Build: 1.11b919904<br/>
Size: 3.10mb<br/>
Uploaded: 2025-03-04T18:11:42.492251+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>Fixes crash in querying details</li></ul>

### v1.10<br/>
Download: <b><a href="https://kustom.rocks/download/kweather/110917814/google_release">Google Play APK</a></b><br/>
Build: 1.10b917814<br/>
Size: 3.10mb<br/>
Uploaded: 2025-03-04T18:11:56.883987+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>Fixes providers cant be selected in the main app</li></ul>

### v1.09<br/>
Download: <b><a href="https://kustom.rocks/download/kweather/109914413/google_release">Google Play APK</a></b><br/>
Build: 1.09b914413<br/>
Size: 3.07mb<br/>
Uploaded: 2025-03-04T18:12:01.662926+00:00<br/>

Release Notes:
<ul style="margin: 0"><li>Support for UV Index and Clouds</li><li>Fixed hourly weather in Accu</li><li>Fixed wind speed in Accu</li><li>Small other fixes</li><li>Language update</li></ul>

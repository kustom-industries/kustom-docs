---
title: Send variables to Kustom via Broadcast
type: docs
categories:
  - Developers
tags:
  - Developers
  - send variables
  - broadcast
---

# Send variables to Kustom via Broadcast

Kustom from version 2.09 allows third party apps to send variables via broadcasts. In order to send a broadcast to Kustom you will need this static entries defined in your code:

```
public static final String KUSTOM_ACTION = "org.kustom.action.SEND_VAR";  
public static final String KUSTOM\_ACTION\_EXT_NAME = "org.kustom.action.EXT_NAME";  
public static final String KUSTOM\_ACTION\_VAR_NAME = "org.kustom.action.VAR_NAME";  
public static final String KUSTOM\_ACTION\_VAR_VALUE = "org.kustom.action.VAR_VALUE";  
public static final String KUSTOM\_ACTION\_VAR\_NAME\_ARRAY = "org.kustom.action.VAR\_NAME\_ARRAY";  
public static final String KUSTOM\_ACTION\_VAR\_VALUE\_ARRAY = "org.kustom.action.VAR\_VALUE\_ARRAY";
```

Then you will need to send a broadcast always providing an "EXT_NAME" and then providing either a single name/value pair or an array of them. So, for example, if we have an application called "foo" that wants to send "myvar" with value "myvalue" you will do something like this:

Intent intent = new Intent(KUSTOM_ACTION);  
intent.putExtra(KUSTOM\_ACTION\_EXT_NAME, "foo");  
intent.putExtra(KUSTOM\_ACTION\_VAR_NAME, "myvar");  
intent.putExtra(KUSTOM\_ACTION\_VAR_VALUE, "myvalue");  
getContext().sendBroadcast(intent);

Then in Kustom you will have to use `$br(foo, myvar)$` to access the data. Finally, if you want to send more than one variable at a time you can use the VALUE\_ARRAY and NAME\_ARRAY extras, just ensure the amount of entries is the same on both.
---
title: Load a preset in the editor from external app
type: docs
categories:
  - Developers
tags:
  - Developers
  - load preset
  - external app
---

# Load a preset in the editor from external app

Starting from 3.16 Kustom allows external apps to directly open a preset into the editor, this is very useful for Dashboards or preset packs in generals. So, how to do that?

### Build a Kustom Uri

You will need to pass to Kustom a specific Uri via intent, in order to build the Uri you will have to follow this format:  
  
`kfile://pkgname/folder/filename`  
  
So, for example, if you preset pack package is `org.great.kustom.pack` and you want to open a wallpaper called `foo.klwp` then your final Uri will be `kfile://org.great.kustom.pack/wallpapers/foo.klwp`. Ok, then how to launch the intent? That's different between apps, see below.

### KLWP

For Kustom Live Wallpaper you will have to do the following:  
  
`Intent i = new Intent();`  
`i.setComponent(new ComponentName("org.kustom.wallpaper",`  
`                        "org.kustom.lib.editor.WpAdvancedEditorActivity"));`  
`i.setData(Uri.parse("kfile://..."));`  
`startActivity(i);`

### KWGT

In Kustom Widget Maker please do this:  
  
`Intent i = new Intent();`  
`i.setComponent(new ComponentName("org.kustom.widget",`  
`                        "org.kustom.widget.picker.WidgetPicker"));`  
`i.setData(Uri.parse("kfile://..."));`  
`startActivity(i);`
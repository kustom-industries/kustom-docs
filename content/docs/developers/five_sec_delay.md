---
title: Launchers touch features support (aka remove '5' secs delay)
type: docs
categories:
  - Developers
tags:
  - Developers
  - 5 second
  - touch delay
---

# Launchers touch features support (aka remove '5' secs delay)

When you press "home" Android will introduce a 5 secs delay to any app and service trying to execute an intent unless the app sending that intent is the current launcher. This prevents Kustom Wallpaper to launch apps without delay since it is running as a service.  
  
This can be easily removed by adding support within the launcher for trusted wallpapers in order to let them remove this delay via a content provider. In order to support this what you should do is the following:

* Create a content provider that will respond to "delete" query like `context.getContentResolver()``.delete(``"content://mypackage/reset5secs") `you can find an example in [this gist](https://gist.github.com/frmz/669eeca0b20b943b7091b9078eb3247e)
* When query is received you should verify via [getCallingPackage](http://developer.android.com/reference/android/content/ContentProvider.html#getCallingPackage()) that the requester is the currently set wallpaper or, if you prefer, Kustom Wallpaper package (org.kustom.wallpaper)
* If that is confirmed you should launch a transparent activity that will call finish() immediately
* If that has been done return 1 otherwise 0

If you implement this feature please then open a ticket using this site to inform the author about the exact URI so it will be added to the app as supported.

Launchers currently supporting this feature are:

* [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher&hl=en)
* [Smart Launcher](https://play.google.com/store/apps/details?id=ginlemon.flowerfree)
* [Lightning Launcher](https://play.google.com/store/apps/details?id=net.pierrox.lightning_launcher_extreme&hl=en)
* [Total Launcher](https://play.google.com/store/apps/details?id=com.ss.launcher2&hl=en)
* [SquareHome 2](https://play.google.com/store/apps/details?id=com.ss.squarehome2&hl=en)
* [Action Launcher 3](https://play.google.com/store/apps/details?id=com.actionlauncher.playstore&hl=en)
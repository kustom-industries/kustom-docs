---
title: How to do a clean Kustom installation
type: docs
categories:
  - How To
tags:
  - How To
  - clean install
  - installation
---

# How to do a Kustom clean installation

1. Uninstall your Kustom app from your device's app page
2. Open your File Explorer
3. Go to Internal/External storage >> Kustom or to which ever custom location you've set
4. Move all the contents of the "Kustom" folder to a backup location
5. Delete the "Kustom" folder from step 3
6. Create a new empty "Kustom" folder in the root directory of your Internal storage
7. Restart the device
8. Re-install your Kustom app from the Play Store
9. Select the empty "Kustom" folder during the initial setup as the main storage location

** If you're using multiple Kustom apps, doing the clean install may affect all of them if they all point to the same Kustom folder.

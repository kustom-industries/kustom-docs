---
title: Create a Font Icon pack
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - font
  - icon pack
---

# Create a Font Icon pack

Kustom has a wonderful Font Icon module that allows you to easily add vector icons to your preset. Sometimes however builtin packs are not enough and you want to add your own, this is very easy, please follow these steps:

* Go to the [IcoMoon App site](https://icomoon.io/app/) and create a new project (top right corner)
* Add your icons and please pay attention to the license!
* Once you are done adding icons press the "Generate Font" button (bottom right corner)
* Give the icons the appropriate name (so just the text right to the icon, no need to change the code)
* Finally press "download" (bottom right corner)
* Copy the ttf font file inside the zip in /Internal storage/Kustom/icons/myfontpack.ttf
* Copy the "selection.json" file inside the zip in /Internal storage/Kustom/icons/myfontpack.json
* Please ensure that json and ttf files have the same name
* You are done! Enjoy your icon pack!

If, in the future, you want to open this project again on IcoMoon site to add new icons just create a new project, press "import" and select the json file, this will load everything including the icons.  
  
P.S. there is a HUGE pack of font icons ported to Kustom available on [this G+ post](https://plus.google.com/115754732439389431992/posts/1mcknWhZsDg), check it out!

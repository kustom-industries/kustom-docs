---
title: Weather Icon Komponent Creation (Image or SVG)
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - weather
  - icon
  - SVG
---

# Weather Icon Komponent Creation (Image or SVG)

Kustom includes a very basic Weather Image Icon Komponent that allows you to show an image based on the current weather condition (or a forecast by changing the "icon" global to "wf(icon, X)" where X is the number of days, 0 for today and so on)...  
  
The Komponent is very easy to edit and has meaningful image file names so you can create your own in a few steps:

**From PC:**

* Download the Komponent from [WeatherIconImage.komp.zip](/files/WeatherIconImage.komp.zip) 
* Uncompress it somewhere or open it with 7zip
* Replace all the images in the "Bitmaps" folder keeping the same names (if you are using JPG or WEBP images just change the extension to PNG, Kustom will take care of the format)
* Edit the "preset.json" file and change Author / Title (optional)
* Save the Zip
* Done, redistribute or reuse!

**From the device:**

* Add the Komponent from your SD card (or from the builtins)
* Unlock the Komponent to edit it pressing the "lock" icon on top right
* Go inside the Komponent and in the "globals" tab at the end
* Pick a new image for every state listed (so enter the "Tornado" image, press "pick image" and choose the corresponding image, then repeat for all, you will NOT be able to see the images once applied)
* Export with your details / name / description
* Done!

SVG (vector) version:  

* Instead of using the "WeatherImage" komponent as a starting point you can use the SVG komponent at [WeatherFlatSVG.komp.zip](/files/WeatherFlatSVG.komp.zip) 
* Follow the same steps as above but use SVG files instead of images
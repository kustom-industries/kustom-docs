---
title: Create a customizable pattern background
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - pattern
  - background
  - customize
---

# Create a customizable pattern background

With Kustom is very very easy to create your own pattern based backgrounds by using the "tiling" function the the [Overlap Group](https://docs.kustom.rocks/docs/general_information/groups_explained/), let's see how to setup a simple pattern with colors customizable via [Globals](https://docs.kustom.rocks/docs/how_to/use_globals/). Steps are detailed just after the video.  
  
[![Kustom: pattern background creation](https://i.ytimg.com/vi/9LRtFtT3YMY/maxresdefault.jpg)](https://www.youtube.com/watch?v=9LRtFtT3YMY "Kustom: pattern background creation")
  
Steps performed in the video:

* Create two global colors, we call them "one" and "two"
* Create an Overlap Group
* We add to the group a background square, we switch color preference to a global by selecting it and clicking the "globe" and we select the "one" color
* We add a small square inside and a circle on top left using global color "two" with the method above
* We go back and we enable tiling with Layer -> Tiling 
* We created our pattern background!
* Finally we want to add some animations to make the pattern scrollable, we do that by using the "Animations -> Add" feature and we setup a scroll animation
* Done, enjoy :)
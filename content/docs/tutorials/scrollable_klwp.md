---
title: How to make scrollable theme in KLWP
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - klwp
  - scrollable
---

# How to make scrollable theme in KLWP.

This tutorial will demonstrate how to create a basic 3-page scrollable live wallpaper in KLWP.

<img src="https://media.giphy.com/media/WH6mXbtHq3mFlhmKYg/giphy.gif" alt="Empty theme" width="200px">

### Requirements:

1. Launcher that supports wallpaper scrolling (eg. Nova Launcher)
2. Enable wallpaper scrolling (Nova settings >> Home screen >> Enable "Wallpaper scrolling")
2. A background image that's bigger than your screen

### How to:

1. Open KLWP and start an empty theme
<br/>
<img src="https://imagizer.imageshack.com/img922/2104/CtT1KY.jpg" alt="Empty theme" width="200px">

2. Go to the Background tab
<br/>
<img src="https://imagizer.imageshack.com/img924/2897/mrnxjF.jpg" alt="Empty theme" width="200px">

3. Change the type to "Image"
<br/>
<img src="https://imagizer.imageshack.com/img923/9400/i9pZku.jpg" alt="Empty theme" width="200px">

4. Tap on "Pick Image"
<br/>
<img src="https://imagizer.imageshack.com/img923/9456/ZkyTfa.jpg" alt="Empty theme" width="200px">

5. Select your background wallpaper
6. Add 3 different Overlap Groups and named them accordingly (Screen1, Screen2...)
<br/>
<img src="https://imagizer.imageshack.com/img924/1299/3iQGdZ.jpg" alt="Empty theme" width="200px">

7. For each Overlap Group, add a Text element with its specific values (Screen 1, Screen 2, Screen 3)
<br/>
<img src="https://imagizer.imageshack.com/img922/5848/kbomQC.jpg" alt="Empty theme" width="200px">

8. For each Overlap Group, add an Animation with the following attributes:
<br/>
<img src="https://imagizer.imageshack.com/img923/5643/VcLiAJ.jpg" alt="Empty theme" width="300px"> <img src="https://imagizer.imageshack.com/img924/6281/aKsL2f.jpg" alt="Empty theme" width="300px"> <img src="https://imagizer.imageshack.com/img922/9389/qg1ugo.jpg" alt="Empty theme" width="300px">

***All information about the animation attributes can be found [here](https://docs.kustom.rocks/docs/general_information/animations_explained/).

9. Save and Apply the theme
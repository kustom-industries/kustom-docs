---
title: Floating Action Button tutorial
type: docs
categories:
  - Tutorials
tags:
  - Tutorials
  - floating
  - action button
---

# Floating Action Button tutorial

## Final result (full video tutorial at the end)

Full KLWP preset with the final result can be downloaded [in this XDA thread](http://forum.xda-developers.com/android/themes/klwp-create-material-animated-floating-t3052886/post59395396#post59395396)  

### Step 1: create the FAB button that will trigger the animation

* Start from an empty preset
* Create a global that will be used to trigger the animations (Globals -> Add -> Type Switch), this global will be used to control animations around
* Add the Image for the FAB, change its name to better see it in the list
* Load an Image that you like, here i am using a generic "plus" button
* Set single Touch action to "Global Switch" and pick the global we just defined ("FAB"), so when touching this image that global will be toggled on/off
* Go to animations and add a new one, select "ReachOn -> Global Switch" so it will be triggered by the global switch, set speed to 5 to make it faster (5 is 50ms), set type to Rotate and amount to "63" so it will not rotate completely and stop a bit earlier looking like an "X" instead of a "+" (you can test the animation pressing the "play" button on top)
* To make it a bit fancier add another animation triggered by the same global, set speed to 5 as above and select "Color Filter Out", this will desaturate the image on click, making it black and white when opened
* Your button is done! Save, go back to screen and see it in action. Cool eh?

### Step 2: create app button

* Add another Image object, change its name to "Facebook" or whatever you want
* Add a Switch animation of type Scroll so the Image will move when switch triggers, set speed at 5 as above, and play with "speed" and the "play" button on top right until you reach the desired result, set also the "angle" param if you want to change scroll direction and mode to "overshoot" if you want to bounce when opening
* Add another Switch animation of type "Fade In" so the item will fade in when switch changes (and while fades), the item will disappear because its initial state will be invisible, if you click the play button you will see it become visible
* Go back to the root and check with the global switch (changing it) if the animation is ok, if its ok change the position of the Facebook button and place it on top of the list so it will go below the FAB (so it will look like it appears from the back)
* Done! Save and test it on screen

### Step 3: create additional buttons

* Copy the facebook button using copy / paste
* Change the name and the image (in this case we use Youtube)
* Go to animations and change the scroll "speed" parameter to make it go farther, test it with play (you will just see an empty red rect because the item is transparent but it will be enough to tune speed
* Save, done!

### Video

[![KLWP Floating Action Button Tutorial](https://i.ytimg.com/vi/-UA1kIZcCp4/maxresdefault.jpg)](https://www.youtube.com/watch?v=-UA1kIZcCp4 "KLWP Floating Action Button Tutorial")
---
title: Math operators in formula
type: docs
categories:
  - More
tags:
  - More
  - math
  - operators
---

# Math operators in formula

As you probably already know kustom supports advanced formulas between $ symbols, what you probably don't know is that, within those symbols, you can also use math operators like "+", "/" and so on, and also use parentheses when needed like you do in math.  
  
The operators will work differently depending on the way you use them, so, let's give some example:

* `$2*(2+3)$` will give 10, this is simple math
* `$df(s)*2$` will return the number of seconds in current minute and multiply it per 2
* `$"foo" + "bar"$` will merge "foo" string and "bar" string resulting in "foobar" text, this is useful when you want to join text sentences and numbers
* `$ai(sunset) - ai(sunrise)$` will subtract 2 dates and return the number of seconds between them, in general a date will be converted in seconds by operators so you can then use it in the formulas 

If you need more advanced functions like "floor", "ceil" and "sqrt" please look into the `mu()` function.
---
title: Kustom Adaptive Icon Contest
type: docs
categories:
  - More
tags:
  - More
  - adaptive icon
  - contest
---

# Kustom Adaptive Icon Contest

Hi! How are you? If you are reading this page then you are interested in the new Kustom adaptive Icon contest, yeah, we all love the good old Kustom icon by Bill Surowiecki but time has come for a redesign to support new Android standards and, as everyone knows, Frank design capabilities sucks very very much so here we are!

Are you in? Not yet? Lets get some rules clarified:

* All the designers willing to participate need to create two sample icons, one for KWGT and one for KLWP (guidelines below)
    
* Deadline is the 31st of October 2018 (uuuuh scary) no exceptions!
    
* A survey will be held in November to ask users to vote the best (poll link will be sticky on ALL kustom G+ communities for 7 days)
    
* Based on survey results and **my own judge Dredd like judgement** i will fill a scoring board and decide the winners
* First 5 will get 50 pro keys for each Kustom app to promote their content (keys will have 1 year limit)
    
* Since good work needs to be paid winner will get 500$ after providing all the necessary icons (details below under winner agreement) and off course be linked anywhere possible as the icon author, i know its not a lot but thats the best i can provide as a time compensation, payment will be subject to an invoice and made via PayPal
    

Icon guidelines
---------------

* Must email their work to [frank.bmonza@gmail.com](mailto:frank.bmonza@gmail.com) with the words "Icon Contest" on subject, i might not see replies on G+ so email also for info or questions
    
* Must support and follow Android Adaptive Icons guidelines: [https://developer.android.com/guide/practices/ui\_guidelines/icon\_design_adaptive.html](https://developer.android.com/guide/practices/ui_guidelines/icon_design_adaptive.html)
    
* Must be really adaptive as above (so in XML with background and foreground), animation is optional, **not complying icons will not be listed in the survey**
    
* Must keep the Kustom main logo idea (the K made by a pipe and a parentheses) "|{", **you do not need to use the same font**, just keep the idea so the logo is still recognizable, if you want the original AI file is [HERE](https://drive.google.com/open?id=0B_QpvSa-tFklYlNoLWRiOFNJS0E) the logo alone can be seen [HERE](https://drive.google.com/open?id=0B_QpvSa-tFklekVPUVJzREhZN3c) and in SVG [HERE](https://drive.google.com/open?id=1z7fi0Mmt2OLTG6tWRZcjJj3Bit6EyUIV)
    
* Icons must have the logo, use the contrast color as the main BG color and associate a smaller icon inside the icon itself to distinguish between the apps, the icon can also be text (like "wallpaper") or both, an example could be the Google Translate icon which has both the Google logo (the "G") and the Translator symbol (the Japanese char) or the current icon which has the Icon and then the text. Point is that the color is not enough to distinguish between the apps (you can also use the background shape for example)
    
* Following variants must be provided, for the contest please provide a 1024x512 PNG image with both side by side in 512x512 format
    
    * KLWP contrast color #CE3645 icon should have the K logo along with an icon that suggest this is a live wallpaper or "wallpaper" text
        
    * KWGT contrast color #455F9E icon should have the K logo along with an icon that suggest this is a widget or "widget" text
        
* **You must have the right to use and distribute everything you use for the Icon, any font, logo or picture used must be suitable for commercial use**
* **I might not add the icon to the contest if i dont feel is up to the standards or for some reason i do not like it**

Winner agreement
----------------

* Must reply via email to [frank.bmonza@gmail.com](mailto:frank.bmonza@gmail.com) with the words "Icon Contest" on subject, i might not see replies on G+ due to too many notifications!
    
* Original files (for each variant), so Adobe Illustrator .ai files, Photoshop, SVG, whatever you used :)
    
* Agreement that i can use the icons for both commercial and non commercial use including modified versions of them and a statement that you own every rights on them
    
* I might be requesting some variation from the original work in case i think there are small things to change
* Following variants should be provided (9 8 total)
    
    * [KLWP](https://play.google.com/store/apps/details?id=org.kustom.wallpaper) contrast color #CE3645 icon should have the K logo along with an icon that suggest this is a live wallpaper or "wallpaper" text
        
    * [KWGT](https://play.google.com/store/apps/details?id=org.kustom.widget) contrast color #455F9E icon should have the K logo along with an icon that suggest this is a widget or "widget" text
        
    * [KLCK](https://play.google.com/store/apps/details?id=org.kustom.lockscreen) contrast color #EE673E icon should have the K logo along with an icon that suggest this is a lockscreen or "lock" text
        
    * KWCH contrast color #5AA875  icon should have the K logo along with an icon that suggest this is a watchface or "watch" text
        
    * [Weather plugin](https://play.google.com/store/apps/details?id=org.kustom.weather) contrast color #88C1D7 icon should have the K logo along with an icon that suggest this is a weather plugin or the "weather" text
        
    * [Unread plugin](https://play.google.com/store/apps/details?id=org.kustom.unread) contrast color anything icon should have the K logo along with an icon that suggests this is an unread plugin or the "unread" text
        
    * [APK Maker](https://play.google.com/store/apps/details?id=org.kustom.apkmaker) app contrast color anything icon should have the K logo along with an icon that suggests this is an APK Maker or the "apk" text
        
    * G+ community one, free to decide contrast color, icon should have the K logo and suggest this is a community
        
    * Generic icon for website, reddit and so on, dark gray contrast color #434343 with the K logo and a generic icon or the "kustom" text
        

If you have questions just email me at [frank.bmonza@gmail.com](mailto:frank.bmonza@gmail.com) using the same subject
---
title: "Crescent Moon Shape"
type: docs
categories:
  - Recipes
  - Shapes
tags:
  - path
  - astronomy
  - moon
  - svg
---
# Crescent Moon Shape
This recipe explains how to combine formulas and paths to create a dynamic shape in Kustom

## Step 1: Create a new Shape Module
In your Kustom editor, create a new Shape module. You can do this by:

1. Tapping the "+" button
2. Selecting "Shape"

In the shape module, select "Path" as the shape type. This will allow you to define a custom shape using SVG path commands.

## Step 2: Define the Path

Here comes the crucial part. You need to define the path that forms the shape of the crescent moon. Given Kustom's viewport of 100x100, we can use the following path:

```
M 50,50 m -25,0 a 25,25 0 1,0 50,0 a 25,$ai(mill)$ 0 1,0 -50,0 Z
```

This will start at the center of the viewport (50,50), move 25 units to the left, then draw two arcs to form the crescent shape. The $ai(mill)$ is the current moon fill percentage, which will control the size of the inner arc, giving us a dynamic crescent shape that changes with the moon phase. We add a "Z" at the end to fully close the path.

## Step 3: Style Your Crescent Shape

You can now style your crescent shape as desired. You can change the color, add a stroke, adjust the opacity, and more.

And there you have it! You've created a dynamic crescent moon shape in Kustom. Happy customizing!
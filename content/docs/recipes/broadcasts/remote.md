---
title: Send a message to Kustom from the internet
type: docs
categories:
  - Recipes
  - Broadcasts
tags:
  - advanced
  - messages
  - http
---
{{< script >}}
  $(document).ready(function(){
    const searchParams = new URLSearchParams(window.location.search)
    const token = searchParams.get('token').trim()
    if (token) {
      $("#token_info_auto").show();
      $("#token_info_manual").hide();
      $(".token").text(token);
      $("span.s1:contains('from kustom settings')").text(function(i, v) {
        return v.replace('your app token from kustom settings', token);
      });
    }
    $("#send_msg").click(function() {
      const data = {}
      data[$("#vname").val()] = $("#vval").val()
      console.log(`Sending to ${token} ${JSON.stringify(data)}`)
      // Send msg
      $.post(
        'https://api.kustom.rocks/msg',
        { 
          tokens: [ token ], 
          data: data  
        },
        function(data, status, jqXHR) {
          $('#msgresult').text('Result: ' + status + ', data: ' + data);
        }
      )
      // Ignore button
      e.preventDefault();
    });
  });
{{< /script >}}

# Send a message to Kustom via HTTP POST
Starting from version 3.70 you can send variables from the Internet directly to Kustom via HTTP POST. This could be 
useful in many situations like:
  - Sending data from your home automation back to your Widget / Wallpaper
  - Integrate with external systems like IFTT or public services
  - Receive alerts from some cloud based service

## How do i find my token?
<div style="display: none" id="token_info_auto">
You are coming from Kustom settings, nice! You can see your device token below:
  <div class="highlight">
    <pre tabindex="0" class="chroma"><code data-lang="token"><span class="token">Your Token</span></code></pre>
  </div>
<!--<h3>Test it now</h3>
You can also send a message from here. So to get value from <code>$br(remote, foo, bar)$</code> use foo as var name and
bar as var value.
<pre><label for="vname">Var name  </label><input type="text" id="vname" name="vname">
<label for="vval">Var value </label><input type="text" id="vval" name="vval">
<button id="send_msg">Send</button></pre>
<span id="msgresult"></span>-->
</div>
<div id="token_info_manual">
To find your app token:
<ul>
  <li>Go to Kustom app Settings</li>
  <li>Press "Advanced Settings"</li>
  <li>Select "Remote Message"</li>
  <li>A dialog will appear, just press COPY</li>
</ul>
</div>

## Does it work on AOSP/Huawei?
No this function right now works only on Google version

## Test with curl
You can test this with CURL on your local machine, let's say you want to set var foo to some value, then in Kustom you
will use the formula ```$br(remote, foo)$```, then use this command to change it:
```bash
curl -X POST https://api.kustom.rocks/msg \
  --header "Content-Type: application/json" \
  --data '{
    "tokens": [
      "your app token from kustom settings",
    ],
    "data": {
      "foo": "hello from remote!"
    }
  }'
```

## Use with IFTT
Let's say for example that you want to update a widget every time Elon tweets, then:
  - Create a new [IFTT](https://ifttt.com/create) task
  - Select Twitter as a trigger, new tweet from specific user, connect Twitter and proceed
  - Press "then" and select "WebHook" as a service
  - Press "make a web request"
  - In the URL field use ```https://api.kustom.rocks/msg```
  - Select method "POST" and content type "application/json"
  - Finally in the body add following json:
```json
{
  "tokens": [
    "your app token from kustom settings"
  ],
  "data": {
    "tweet": "{{Text}}"
  }
}
```
You are done! Just use ```$br(remote, tweet)$``` in Kustom now and wait for the next tweet!

## Fair use
This API is currently rate limited you can do up to 5 calls **per minute**, you can see how many calls you have left by looking at the `X-RateLimit-*` headers during the call. I might increase / decrease this number based on usage.



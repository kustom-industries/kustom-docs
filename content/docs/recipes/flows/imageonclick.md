---
title: Get a random image on click
type: docs
categories:
- Recipes
- Flows
tags:
- flows
- images
- random
- wallpaper
---

# Get a random image on click
Flows can react to different things including clicks, in this example we will see how to trigger a Flow by clicking an
item and making the flow pick a random file from a folder and show the image on the widget

## Tutorial
{{< col >}}

### Steps
First of all add a Widget (or a wallpaper or any other Kustom item), then:
- Add an Image Module, scale to desired size
- Add a Text global, call it "bmp"
- Go back to the Image Module, set Bitmap property to a Formula then use $gv(bmp)$ as the value to point to the new global
- Go back to the Layer and in the Flows tab
- Create a new Flow
- Add a Trigger of type Manual (which means triggered on touch)
- Add an action to pick files from folder, select the right folder and image as the filter
- Add another action to set a Global, select the global we created previously
- Save the flow
- Go back to the Image item, go to the touch tab
- Add a touch action of type trigger flow and select the flow
- You are DONE

<--->

### Video
{{< yt FHHLvpvcrK4 "9:18" >}}

{{< /col >}}



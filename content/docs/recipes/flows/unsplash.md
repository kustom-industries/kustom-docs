---
title: Random image from Unsplash on click
type: docs
categories:
  - Recipes
  - Flows
tags:
  - flows
  - images
  - random
  - wallpaper
  - unsplash
  - api
---

# Random image from Unsplash on click
In this simple example #CraftMath downloads a random image JSON from Unsplash using their APIs updating it everything
time a click is triggered

## Tutorial

### Video
{{< yt 6pxfkhv7YsI standard medium >}}

### Steps
First of all add a Widget (or a wallpaper or any other Kustom item), then:
- Register Unsplash account and request an API key 
- Store API key as a "secret" or normal global, name it "key"
- Create a new text global, name it "json"
- Go to Flows, add a new Flow
- Add a "manual trigger" this will trigger the flow manually via touch
- Add an Action of type WebGet and use "https://api.unsplash.com/photos/random?client_id=$gv(key)$" as URL
- Add an action to store to a Global, select "json" as the global, flow will download the JSON file and assign the 
global to the local JSON file path (unless you switch "store file content not path" which will intead store the file
content in the global)
- Finally add an image, switch "Bitmap" to a formula and use $wg(gv(json), json, .urls.raw)$ to use the image url from
the json
- You are done

---
title: KLCK FAQ
type: docs
categories:
  - Common Issues
tags:
  - Introduction
  - KLCK
  - FAQ
---
# KLCK Frequently Asked Questions

## How to apply KLCK

When you don’t see your KLCK lock screen, sometimes, the simple reason is that you have not actually enabled/applied it. Unlike KWGT and KLWP, this needs to be manually enabled in KLCK. To do this:

{{< col >}}

### Steps

1. Open KLCK editor
2. Make sure you're in edit mode
3. Open the side menu
4. Enable the "Lock Screen" toggle switch

<img src="https://imagizer.imageshack.com/img922/3744/6B3olc.gif" alt="KLCK Toggle" width="300">

<--->

{{< /col >}}

## Why KLCK needs to display a Notification?

Kustom to work properly needs to be always running, the notification can be disabled by removing Kustom from battery optimized apps, a full explanation is available [here](https://docs.kustom.rocks/docs/general_information/persistent_notifciation/).

## Is KLCK a secure Lock Screen?

No it is NOT, the main reason is that there is no official way to create an alternative lockscreen in Android, what Kustom does is just displaying on top of your system lockscreen and request an unlock when you unlock Kustom

## Does KLCK support Fingerprint Unlock?

Yes it does but to avoid conflicts with the system lockscreen it can do that only if system lockscreen is not secure OR screen is off

## Can i use my own fonts?

Sure! In your SD card you should find a directory called Kustom with a folder named "fonts" inside, put your ttf/otf items there, they will then be listed in the editor.

## Someone sent me a file in ".klck" or ".klck.zip" format, where do i put it?

Just press "Load Preset", then the "folder" button on top menu, then select "Import" from the options. If you want to do that manually or from PC you need to copy the file as it is (without decompressing) in the SD card under "Kustom/lockscreens/" directory, the directory should already be there. Once copied the wallpapers will show up in the "sd card" section when pressing "load preset" from the main settings. Please always ensure that the zip file is not corrupted and that the extension has not been changed (it has to be ".klck.zip" or ".klck", both are the same)

## How much battery does Kustom use?

Short answer: almost zero. Long answer: Kustom is running only when you are looking at it and even when active it updates once every second so its very efficient. On top of that Kustom will never update unless strictly necessary (if you are not scrolling or data is not changing). This off course applies to the lockscreen, the editor is a different story since it needs some CPU to show you the changes while doing them so it will use some battery if you edit your template a lot. If you want to have details on how it works i suggest to use BetterBatteryStats, you will have then detailed info on the battery usage of the lockscreen (org.kustom.lockscreen.service) and the editor (org.kustom.lockscreen).

## And what about RAM?

Linux motto is "free RAM is wasted RAM", when memory is available and not used by anyone KLCK will use it to cache its objects and make things smoother and use less CPU as possible (so less Battery drain), however, as soon as the system needs it KLCK will reduce its usage as much as possible (this is why sometimes coming back from heavy memory apps like browsers KLCK will slow down a bit the first second or two). So, depending on preset complexity, KLCK might use quite a bit of memory, but this is absolutely normal and won't cause any issue.

## Why you support Android KitKat or better only?

Kustom uses features that are only available from Android 4.4 and cannot be ported to a lower version

## Can i animate items inside a group?

Animations inside groups are not supported for technical reasons

## Next alarm always returns random values

If you are using a third party Alarm software then your app might not be supported or it might be caused by Tasker if you checked the option "Use Reliable Alarms" in preferences (in this case you need to disable it to have reliable results).

## My preset was using Google Maps and now is showing just blank

Unfortunately some time ago Google has removed the ability to fetch maps data without using a Google Maps API key, there is no solution to this other than registering an API key and then use it inside the maps call. Probably in the future theme owners will add the ability to add an API key or embed their own but so far you would need to do it manually to make it work.

## I cannot show KLCK behind my Notch / Cutout

Please ensure Kustom is set to cover entire screen, you can usually change that in the Android Settings under Display section, just find KLCK there and set it in full screen mode, that's it!

## I cannot use custom fonts anymore on my Huawei phone

On Huawei phones there is a Theming app, please ensure that you did not change the font there, if you did then switch it back to the default one. Unfortunately the way Huawei implemented theming is by overriding core OS functionatlies
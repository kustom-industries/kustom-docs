---
title: KLWP FAQ
type: docs
categories:
  - FAQ
tags:
  - Introduction
  - KLWP
  - FAQ
---
# KLWP Frequently Asked Questions

## Wallpaper is not scrolling!

Please ensure that you have "background scrolling" enabled in your Launcher settings, for example, in Nova, you can find this in "Settings -> Desktop -> Wallpaper Scrolling". Then ensure that the image you set as background is larger then your screen (if you cropped it to screen size it wont scroll because there is nothing to scroll). Finally ensure that the number of screens in your launcher have the same count as the ones on the preset you are using. On some Huawei phones you need to go back to EMUI launcher (if its not your Launcher already), select a picture as a background and select the scrolling option on the bottom right, then go back to your Launcher of choice and KLWP. 

## What does "5 secs delay see FAQ" mean?

When you press "home" Android will introduce a 5 secs delay to any app and service trying to execute an intent unless the app sending that intent is the current launcher. This prevents Kustom Wallpaper to launch apps whithout delay since it is running as a service. To avoid this you need to either use the "back" button or use a compatible Launcher such as Nova. If your Launcher is not supported you can send [this link](https://docs.kustom.rocks/docs/developers/five_sec_delay/) to them so that they can develop the proper workaround. If you are rooted you can also checkout the StopSwitchDelay module for XPosed.

## Tap is not working

Tap to Wallpaper must be supported by the Launcher, in some case (es in Lightning Launcher) you need to go into the settings and map something to the Command / Secondary Command action. In case of issues please contact Launcher support.

## Tap works everywhere but not on the bottom of the screen!

In order to have touch actions to work in the bottom area of your launcher you need to have the app dock hidden, this is not available in all Launchers.

## Which launchers support animations?

The app is tested on Google Now Launcher and Nova Launcher but all should work, animations have been reported to have problems in GO Launcher, this is something we cannot fix, please consider contacting GO support and ask them to properly implement Live Wallpaper standards

## Can i use my own fonts?

Sure! In your SD card you should find a directory called Kustom with a folder named "fonts" inside, put your ttf/otf items there, they will then be listed in the editor.

## Someone sent me a file in ".klwp" or ".klwp.zip" format, where do i put it?

Just press "Load Preset", then the "folder" button on top menu, then select "Import" from the options. If you want to do that manually or from PC you need to copy the file as it is (without decompressing) in the SD card under "Kustom/wallpapers/" directory, the directory should already be there. Once copied the wallpapers will show up in the "sd card" section when pressing "load preset" from the main settings. Please always ensure that the zip file is not corrupted and that the extension has not been changed (it has to be ".klwp.zip" or ".klwp", both are the same)

## How much battery does Kustom use?

Short answer: almost zero. Long answer: Kustom is running only when you are looking at it and even when active it updates once every second so its a lot different from standard live wallpapers which do 60 frame per second animations and sometimes also run in background. On top of that Kustom will never update unless strictly necessary (if you are not scrolling or data is not changing). This off course applies to the wallpaper, the editor is a different story since it needs some CPU to show you the changes while doing them so it will use some battery if you edit your template a lot. If you want to have details on how it works i suggest to use BetterBatteryStats, you will have then detailed info on the battery usage of the wallpaper (org.kustom.service) and the editor (org.kustom.wallpaper).

## And what about RAM?

Linux motto is "free RAM is wasted RAM", when memory is available and not used by anyone KLWP will use it to cache its objects and make things smoother and use less CPU as possible (so less Battery drain), however, as soon as the system needs it KLWP will reduce its usage as much as possible (this is why sometimes coming back from heavy memory apps like browsers KLWP will slow down a bit the first second or two). So, depending on preset complexity, KLWP might use quite a bit of memory, but this is absolutely normal and won't cause any issue.

## Why you support Android KitKat or better only?

Kustom uses features that are only available from Android 4.4 and cannot be ported to a lower version

## Can i animate items inside a group?

Animations inside groups are not supported for technical reasons

## Why KLWP says that i reached the maximum number of elements in the root?

Currently KLWP does not support more than 64 complex objects in the root container, simple shapes (like circles and rectangles with non rounded corners and no gradients) are not considered in this sum.

## How can i move an item to another screen?

There is no concept of screens in a live wallpaper but just scrolling, so, by default, everything is always on your screen, if you want something to be only on one screen you need to add an animation and set the center screen to the screen you want. So, for example, if you add a "fade out" animation and set center screen to "3" then your object will fade out when you scroll away from screen "3". You can also use other animations the same way, like scale, scroll and so on. Scroll will act very much like the launcher does by default (so moving the item when you swipe screens).

## Next alarm always returns random values

If you are using a third party Alarm software then your app might not be supported or it might be caused by Tasker if you checked the option "Use Reliable Alarms" in preferences (in this case you need to disable it to have reliable results).

## My preset was using Google Maps and now is showing just blank

Unfortunately some time ago Google has removed the ability to fetch maps data without using a Google Maps API key, there is no solution to this other than registering an API key and then use it inside the maps call. Probably in the future theme owners will add the ability to add an API key or embed their own but so far you would need to do it manually to make it work.

## I cannot show KLWP behind my Notch / Cutout

Please ensure Kustom is set to cover entire screen, you can usually change that in the Android Settings under Display section, just find KLWP there and set it in full screen mode, that's it!

## I cannot use custom fonts anymore on my Huawei phone

On Huawei phones there is a Theming app, please ensure that you did not change the font there, if you did then switch it back to the default one. Unfortunately the way Huawei implemented theming is by overriding core OS functionatlies

---
title: Tutorial List
type: docs
categories:
  - General Information
tags:
  - General Information
  - Tutorial
---

# Tutorial List

## External Tutorials

* A very basis [Video Tutorial](https://www.youtube.com/watch?v=bJEi34O_g0Q) for beginners for KWGT
* A very basic [Video Tutorial](https://www.youtube.com/watch?v=UROFNvQoVaw "Link: https://www.youtube.com/watch?v=UROFNvQoVaw") for beginners for KLWP
* A great [set of intruduction videos](https://www.youtube.com/playlist?list=PLLASdjd8R2RjAAUBAefqvlvNvG64JWp3j) by [Dillogic](https://plus.google.com/+Dillogic)
* Kustom Series on [Jagwar Site](http://www.jagwar.de/kustom-live-wallpaper-maker/ "Link: http://www.jagwar.de/kustom-live-wallpaper-maker/"), very good set of written tutorials for beginners
* Advanced video tutorials on [Brandon Craft YouTube Channel](https://www.youtube.com/user/blc0527/playlists "Link: https://www.youtube.com/playlist?list=PLGVFcC04AlxckOkiwNUbp-QkSEVLBeaHg") a MUST see if you want to fully exploit Kustom

## Basic Tasks

* Add a simple [Photo Widget](https://www.youtube.com/watch?v=UbZC1DjopDI) (video)
* Create your own [Weather Komponent](https://docs.kustom.rocks/docs/tutorials/weather_icon_creation/)
* Create a new [Font Icon Pack](https://docs.kustom.rocks/docs/tutorials/font_icon_pack/)
* Change a Layer or Image if [day or night](https://docs.kustom.rocks/docs/tutorials/switch_layout/)
* Use different colors [based on a condition](https://docs.kustom.rocks/docs/tutorials/change_color/)
* [Floating Action Button](https://docs.kustom.rocks/docs/tutorials/floating_action_button/) tutorial (FAB, KLWP only)
---
title: Publishing Tips
type: docs
categories:
  - General Information
tags:
  - General Information
  - publishing
  - tips
---

# Publishing Tips

To be found via in Play Store search please use the following tags:  
  - **[#KLWP](https://plus.google.com/s/%23KLWP)** for Kustom Wallpaper skins   
  - **[#Komponent](https://plus.google.com/s/%23Komponent)** for Kustom Komponents Pack   
  
If you need help creating an APK skin check this:  
  - [https://bitbucket.org/frankmonza/kustomskinsample/overview](https://bitbucket.org/frankmonza/kustomskinsample/overview)  
  
If you want to market a template i suggest posting it  here:  
  \- G+ official gallery: [https://plus.google.com/communities/105800766530390603634](https://plus.google.com/communities/105800766530390603634)  
  \- Reddit: [http://www.reddit.com/r/androidthemes/](http://www.reddit.com/r/androidthemes/)  
  
If you want a nice animated GIF of your template check out this (you can upload any MP4 video extracted from a phone or even YT links):  
  - [http://gfycat.com/](http://gfycat.com/)  
  
If you want to have a template included in the base pack just submit it here via support ticket specifying it can be added, save it with the correct name so pressing "more from author" in the preset list will correctly bring to the market page. Requirements:  
  \- Ultra minimal  
  \- Scroll / animations ok but it has to work on any number of screens  
  \- Light (< 200k)
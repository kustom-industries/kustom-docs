---
title: Credits 
type: docs
categories:
  - General Inforamtion
tags:
  - General Inforamtion
  - credits
  - translation
---

# Credits

A lot of people is supporting this project and i want to thank them all from the wonderful [G+ community](https://plus.google.com/communities/108126179043581889611) to the users. A special mention goes to:

### Translations

German: [Wladislaw Tauberger](https://plus.google.com/+WladislawTauberger)   
Dutch: [Carla Jacobs](https://plus.google.com/+CarlaJacobs63/posts)   
Japanese: [WedyDQ10](http://androplus.org/)  
Chinese (Taiwan): [Max Yang](https://plus.google.com/107956848004169869398)  
Chinese (China): [Lai Hongjie](https://plus.google.com/u/0/116343305300422554988) and [周尚瑜](https://plus.google.com/105123488908795667901)  
French: [Aurel Rei](https://plus.google.com/109950615487822771272/posts)  
Spanish: [Marco A. Maza Hdez](https://plus.google.com/+MarcoAMazaHdez/posts)  
Romanian: [Marian Cimbru](https://plus.google.com/+MarianCimbru/posts)

### Bug Squashing and Features

[Izzy Lamantia](https://plus.google.com/+IzzyLamantia), [Bill Surowiecki](https://plus.google.com/+BillSurowiecki), [Jeppe Foldager](https://plus.google.com/+JeppeFoldager), [Sebastian Spindler](https://plus.google.com/+SebastianSpindler)  
  
Forgot someone? Just drop me a line using "kontact support" on top right :)
---
title: Music controls and formula not properly working
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - music player
  - controls
---

# Music controls and formula not properly working

There are cases where your music controls are not working properly. There are a couple of reasons why this happens. Make sure to check the following to ensure that the Kustom app has all the right permission and requirements.

- Kustom has access to Notifications (it should ask you when you save). To manually set/check, for most Android devices, go to your device's settings >> Apps >> Overflow icon >> Special access >> Notification access.

- You started your music player at least once (Kustom detects automatically the player being used)

- Ensure the Player properly shows in the notification panel

- If the album art doesn't work, ensure your player, like Poweramp for example, doesn't have the option to hide the cover in the lock screen or notification enabled.

- Try to reboot once, this sometimes is needed when Kustom is upgraded. In the worst case, you can also try reinstalling the app.
In most cases, if the issue is only affecting a specific music player, this is related to the player not implementing Android standards. Usually, a test can be done with Google Music which is fully supported and compliant.

- Additionally, you can manually set your desired music player by going to app settings >> General settings >> Preferred Music Player
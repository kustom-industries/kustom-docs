---
title: Kustom is using a lot of data
type: docs
categories:
  - Common Issues
tags:
  - Common Issues
  - Data usage
---

# Kustom is using a lot of data

So the app itself doesn't use data usually except for weather which shouldnt use almost any. What can use a LOT of bandwidth are presets using live map as background or internet downloaded images, depending on the preset they might have used accurate location to get the image and that might cause the image to update a lot, there quite a few solutions to this if you want to still use those presets:

Ensure location accurancy in the settings is set to the lowest level (no power)
Use a fixed location instead of GPS if you are not interested in location updating automatically
Check in the preset where the formula `$li(lat)$` and `$li(lon)$` are used and replace them with their "low precision" counterpart which is `$li(lplat)$` and `$li(lplon)$`
Hope this helps, if you still have issues let me know!
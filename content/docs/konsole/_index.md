---
title: Konsole
bookCollapseSection: true
type: docs
weight: 200
categories:
  - Konsole
tags:
  - apk
  - appcenter
---
# Konsole
Imagine being able to create and share your Kustom packs with the Kustom community effortlessly. That's exactly what Konsole, enables you to do. This innovative app takes the hassle out of packaging your Kustom presets into APK files, ensuring a seamless experience for both creators and users.

Check it out now:
- Official release on [Play Store](https://kustom.rocks/konsole)
- Manual download [APK](https://kustom.rocks/konsole/apk) 

## Core Features of Konsole

- **Create APK Packs**: With Konsole, you can easily create APK and AAB packs for your Kustom presets. This powerful feature allows you to neatly package your Kustom presets for easy sharing and deployment either on Play Store or via other channels. Konsole backends supports standard Kustom dashboards and creates both APK and AAB files.

- **Future Gallery Upload**: In the near future, you'll be able to upload your APK packs directly to the Kreators Gallery, fostering a more interconnected Kustom community. Certified Kreators will also get a revenue shares from a subscription mechanism (more soon)

- **Supports Standard Wallpapers**: Konsole also supports the inclusion of standard wallpapers inside the pack, providing an extra layer of customization for your Kustom presets.

So whether you're a seasoned Kustom creator or a newcomer eager to share your designs, Konsole is your go-to platform for creating, packaging, and sharing your Kustom widgets, wallpapers, and more.

---
title: "Konsole App Center Upload Token"
type: docs
categories:
  - Konsole
tags:
  - apk
  - appcenter
---
# App Center upload
The App Center Upload Token is a crucial feature in your Konsole app that aids in streamlining the application
deployment process. This article will provide a brief overview of the token, what the App Center does, and how you 
can generate your token and leverage its full potential. 

## What Does the Token Do?

The App Center Upload Token in Konsole is essentially a unique identifier that facilitates the upload and deployment of your Kustom APK files (like KLWP, KWGT, KLCK, and KWCH) to Microsoft's App Center. This token contains pertinent information, such as the username, token ID, application name, and distribution group, all of which are crucial in determining where and how your APK is deployed.

## What is App Center?

[App Center](https://docs.microsoft.com/en-us/appcenter/) is a comprehensive solution for continuous integration and distribution provided by Microsoft. By uploading your packs to App Center you can do the following:
- Provide a public download link to your pack by using the "public group" functionality, howto [here](https://learn.microsoft.com/en-us/appcenter/distribution/groups)
- Automatically publish to Play Store by connecting your Play Account (more info [here](https://learn.microsoft.com/en-us/appcenter/distribution/stores/googleplay))

To start with App Center, you will need to create an account and an app. Microsoft provides a detailed guide on how to do this, which you can find [here](https://docs.microsoft.com/en-us/appcenter/distribution/uploading/#manually-uploading-the-release).

## How to Get the App Center Token?

To obtain the App Center Upload token, you will need to navigate to your account settings within the App Center dashboard. Once there, you can generate a new API token. This [link](https://docs.microsoft.com/en-us/appcenter/api-docs/index?view=app-center-1.0#authentication) provides a comprehensive guide to getting your App Center token.

## Understanding the Konsole Token Format

The Konsole token adopts a distinct format:
`appcenter://user/token?app=appname&group=groupname`

Here's a breakdown of each component:

- User: Mandatory field that represents the App Center username.
- Token: Mandatory field that represents the App Center token.
- App: Represents the app name in App Center. If not provided, Konsole will attempt to infer it using the package name based on the Konsole app name.
- Group: Represents the App Center distribution group. If not provided, Konsole will use the first public group or the first group if no public group is defined.

For instance, a token with only mandatory fields might look like:
`appcenter://AwesomeKreator/token`

And a token with all fields could look like:
`appcenter://AwesomeKreator/token?app=MyGreatApp&group=GroupName`
```
